package com.laidw.chess.entity;

import com.laidw.chess.battle.Player;
import lombok.Data;

/**
 * 用户信息
 *
 * @author NightDW 2023/12/1 16:59
 */
@Data
public class User implements Player {
    private Long userId;
    private String username;
    private String password;
    private int score;

    @Override
    public int hashCode() {
        return userId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof User && ((User) o).userId.equals(userId);
    }

    public static User ofUserId(Long userId) {
        User user = new User();
        user.setUserId(userId);
        return user;
    }
}
