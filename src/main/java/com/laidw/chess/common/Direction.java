package com.laidw.chess.common;

import org.springframework.lang.Nullable;

import java.util.Arrays;
import java.util.List;

/**
 * 上下左右等方向
 *
 * @author NightDW 2023/10/6 18:09
 */
public enum Direction {
    UP(-1, 0),
    DOWN(1, 0),
    LEFT(0, -1),
    RIGHT(0, 1),
    UP_LEFT(-1, -1),
    UP_RIGHT(-1, 1),
    DOWN_LEFT(1, -1),
    DOWN_RIGHT(1, 1);

    public static List<Direction> normals() {
        return Arrays.asList(UP, DOWN, LEFT, RIGHT);
    }

    public static List<Direction> obliques() {
        return Arrays.asList(UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT);
    }

    /**
     * 获取某一方的前进方向（即指向对方的方向）
     */
    public static Direction getForward(Color color) {
        return color == Color.RED ? UP : DOWN;
    }

    /**
     * 判断从from指向to的方向（上下左右）；如果from点和to点是同一个点，或不位于同一行/列，则报错
     */
    public static Direction getNormal(Position from, Position to) {
        if (from != to) {
            if (from.x == to.x) {
                return from.y < to.y ? RIGHT : LEFT;
            }
            if (from.y == to.y) {
                return from.x < to.x ? DOWN : UP;
            }
        }
        throw new IllegalArgumentException();
    }

    public final int dx;
    public final int dy;
    Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    @Nullable
    public Position next(Position position) {
        return Position.ofNullable(position.x + dx, position.y + dy);
    }

    public static void main(String[] args) {
        System.out.println(getNormal(Position.of(5, 5), Position.of(5, 4)));
        System.out.println(getNormal(Position.of(5, 5), Position.of(5, 6)));
        System.out.println(getNormal(Position.of(5, 5), Position.of(4, 5)));
        System.out.println(getNormal(Position.of(5, 5), Position.of(6, 5)));
    }
}
