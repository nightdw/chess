package com.laidw.chess.common;

import org.springframework.lang.Nullable;

/**
 * 棋盘坐标，采用享元模式
 *
 * @author NightDW 2023/10/5 18:56
 */
public final class Position {
    public static final int COLS = 9;
    public static final int ROWS = 10;

    public final int x;
    public final int y;

    /**
     * 将本棋盘坐标转成某一方的相对坐标
     */
    public Relative toRelative(Color color) {
        return color == Color.BLACK ? new Relative(x, y) : new Relative(rotateX(x), rotateY(y));
    }

    /**
     * 获取本坐标与另一个坐标之间的距离（只能上下左右移动）
     */
    public int getDistanceWith(Position other) {
        return Math.abs(x - other.x) + Math.abs(y - other.y);
    }

    @Override
    public int hashCode() {
        return x * COLS + y;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    /**
     * 相对坐标，用于表示红方或黑方视角下的坐标；比如，棋盘的(9, 8)坐标是红方视角下的(0, 0)坐标
     */
    public static class Relative {
        public final int x;
        public final int y;

        public Relative(int x, int y) {
            if (!isValid(x, y)) {
                throw new IllegalArgumentException();
            }
            this.x = x;
            this.y = y;
        }

        /**
         * 判断当前坐标是否在我方的皇宫中
         */
        public boolean isInPalace() {
            return x <= 2 && y >= 3 && y <= 5;
        }

        /**
         * 判断当前坐标是否在我方这边
         */
        public boolean isInSide() {
            return x <= 4;
        }

        /**
         * 将相对坐标转成绝对坐标；color代表该相对坐标是哪方的
         */
        public Position toAbsolute(Color color) {
            return color == Color.BLACK ? Position.of(x, y) : Position.of(rotateX(x), rotateY(y));
        }
    }


    //
    // ====================== 工具方法 ======================
    //

    public static boolean isValid(int x, int y) {
        return x >= 0 && y >= 0 && x < ROWS && y < COLS;
    }

    private static int rotateX(int x) {
        return ROWS - 1 - x;
    }

    private static int rotateY(int y) {
        return COLS - 1 - y;
    }

    public static Position avg(Position p1, Position p2) {
        return of((p1.x + p2.x) >> 1, (p1.y + p2.y) >> 1);
    }

    /**
     * 已知target在horse的马脚上，获取到能别马腿的位置
     */
    public static Position horseFulcrum(Position horse, Position target) {

        // 如果x坐标的变化量为1，则x值取马的x值，y值取平均值即可；否则反过来
        return Math.abs(horse.x - target.x) == 1 ?
                Position.of(horse.x, (horse.y + target.y) >> 1) :
                Position.of((horse.x + target.x) >> 1, horse.y);
    }


    //
    // ====================== 享元模式 ======================
    //

    private static final Position[][] cache = new Position[ROWS][COLS];
    static {
        for (int x = 0; x < ROWS; x++) {
            for (int y = 0; y < COLS; y++) {
                cache[x][y] = new Position(x, y);
            }
        }
    }

    public static Position of(int x, int y) {
        return cache[x][y];
    }

    @Nullable
    public static Position ofNullable(int x, int y) {
        return isValid(x, y) ? of(x, y) : null;
    }

    private Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
}