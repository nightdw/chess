package com.laidw.chess.common;

/**
 * 红黑双方
 *
 * @author NightDW 2023/10/5 1:10
 */
public enum Color {
    RED(0, "\u001B[0;31m%s\u001B[0m"),
    BLACK(1, "\u001B[0;32m%s\u001B[0m");

    public final int value;
    private final String pattern;
    Color(int value, String pattern) {
        this.value = value;
        this.pattern = pattern;
    }

    public Color reverse() {
        return this == RED ? BLACK : RED;
    }

    public String format(Role role) {
        return String.format(pattern, role.getName(this));
    }

    public static Color of(int value) {
        for (Color color : values()) {
            if (color.value == value) {
                return color;
            }
        }
        throw new IllegalArgumentException();
    }
}