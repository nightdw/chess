package com.laidw.chess.common;

/**
 * 棋子类型
 *
 * @author NightDW 2023/10/5 1:10
 */
public enum Role {
    SOLDIER(1, 5, true, "兵", "卒"),
    CANNON(2, 3, true, "炮", "炮"),
    CHARIOT(3, 2, true, "车", "车"),
    HORSE(4, 4, true, "马", "马"),
    ELEPHANT(5, 6, false, "相", "象"),
    GUARD(6, 7, false, "仕", "士"),
    KING(7, 1, false, "帅", "将");

    public final int value;
    public final int sort;
    public final boolean aggressive;
    public final String[] names;
    Role(int value, int sort, boolean aggressive, String... names) {
        this.value = value;
        this.sort = sort;
        this.aggressive = aggressive;
        this.names = names;
    }

    public String getName(Color color) {
        return names[color.ordinal()];
    }

    public static Role of(int value) {
        for (Role role : values()) {
            if (role.value == value) {
                return role;
            }
        }
        throw new IllegalArgumentException();
    }
}