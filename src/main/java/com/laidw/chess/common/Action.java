package com.laidw.chess.common;

import com.laidw.chess.Chess;
import lombok.AllArgsConstructor;

/**
 * 下棋的动作，将某个位置上的棋子移到其它位置
 *
 * @author NightDW 2023/10/7 15:14
 */
@AllArgsConstructor
public class Action {
    public final Position from;
    public final Position to;

    /**
     * 将当前动作解释为棋谱（如车一进二等）；根据棋盘的不同，得到的棋谱也可能不同
     */
    public final String toString(Chess chess) {
        return null;
    }

    @Override
    public String toString() {
        return "[" + from + "->" + to + "]";
    }

    @Override
    public final int hashCode() {
        return from.hashCode() * 31 + to.hashCode();
    }

    @Override
    public final boolean equals(Object obj) {
        return obj instanceof Action && from == ((Action) obj).from && to == ((Action) obj).to;
    }
}