package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.common.Color;
import com.laidw.chess.common.Position;
import com.laidw.chess.common.Role;
import com.laidw.chess.component.PieceCounter;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

/**
 * 棋盘上的棋子
 *
 * @author NightDW 2023/10/5 0:59
 */
public abstract class Piece {
    private static final AtomicInteger nextId = new AtomicInteger(0);

    /**
     * 获取棋子的哈希值，可以用4个比特来表示（最高位代表颜色，后面3位代表棋子类型
     */
    public static int hash(Color color, Role role) {
        return (color.value << 3) | role.value;
    }

    /**
     * 通过哈希值获取颜色
     */
    public static Color parseColor(int hash) {
        return Color.of(hash  >> 3);
    }

    /**
     * 通过哈希值获取棋子类型
     */
    public static Role parseRole(int hash) {
        return Role.of(hash & 7);
    }

    /**
     * 该棋子的id；由于同一方可能有多个相同角色的棋子，因此需要用该字段来区分
     */
    private final int id;

    /**
     * 该棋子的颜色
     */
    @Getter
    protected final Color color;

    /**
     * 该棋子的角色
     */
    @Getter
    protected final Role role;

    /**
     * 该棋子的位置；位置会不断发生变动
     */
    @Getter
    @Setter
    protected Position position;

    /**
     * 该棋子所在的棋盘
     */
    protected final Chess chess;

    /**
     * 该棋子能攻击到的敌方棋子；越靠前的棋子的攻击价值越高
     */
    private final Set<Piece> attacks = new TreeSet<>(Comparator.comparingInt((Piece p) -> p.role.sort).thenComparingInt(p -> p.id));

    /**
     * 该棋子能保护到的我方棋子
     */
    private final List<Piece> protects = new ArrayList<>();

    /**
     * 该棋子被多少个敌方棋子攻击
     */
    @Getter
    protected final PieceCounter attackers = new PieceCounter();

    /**
     * 该棋子被多少个我方棋子保护
     */
    @Getter
    protected final PieceCounter protectors = new PieceCounter();

    /**
     * 该棋子能移动到的位置（包含能攻击到的棋子的位置）
     */
    @Getter
    protected final Set<Position> nextPositions = new HashSet<>();


    //
    // ====================== 简单方法 ======================
    //

    protected Piece(Color color, Role role, Position position, Chess chess) {
        this.id = nextId.getAndIncrement();
        this.color = color;
        this.role = role;
        this.position = position;
        this.chess = chess;
    }

    public final boolean isOpponent(Piece piece) {
        return this.color != piece.color;
    }

    public final boolean canMoveTo(Position to) {
        return nextPositions.contains(to);
    }

    public final String getName() {
        return role.getName(color);
    }

    @Override
    public final String toString() {
        return color.format(role);
    }

    @SuppressWarnings("unused")
    public final String getDetails() {
        StringBuilder sb = new StringBuilder();

        sb.append("id：").append(id).append('\n');

        sb.append("攻击：[");
        if (!attacks.isEmpty()) {
            for (Piece piece : attacks) {
                sb.append(piece.getName()).append(' ');
            }
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append(']').append('\n');

        sb.append("保护：[");
        if (!protects.isEmpty()) {
            for (Piece piece : protects) {
                sb.append(piece.getName()).append(' ');
            }
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append(']').append('\n');

        sb.append("被攻击：").append(attackers).append('\n');
        sb.append("被保护：").append(protectors).append('\n');

        sb.append("能到达的点：[");
        if (!nextPositions.isEmpty()) {
            for (Position nextPosition : nextPositions) {
                sb.append(nextPosition).append(' ');
            }
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append("]\n");

        return sb.toString();
    }


    //
    // ====================== 工具方法 ======================
    //

    /**
     * 当找到一个可达的点时，调用本方法将其保存下来
     */
    protected final void addNextPosition(Position position) {
        nextPositions.add(position);
    }

    /**
     * 当找到一个能攻击或保护到的棋子时，调用本方法将其保存下来
     */
    protected final void addAttackOrProtect(Piece piece) {
        if (isOpponent(piece)) {
            attacks.add(piece);
            piece.attackers.add(this);
            addNextPosition(piece.position);
        } else {
            protects.add(piece);
            piece.protectors.add(this);
        }
    }

    /**
     * 上面两个方法的结合；这里的position可以为null，因此调用者不用关心坐标出界的问题
     */
    protected final void addIfValid(@Nullable Position position) {
        if (position != null) {
            Piece piece = chess.get(position);
            if (piece == null) {
                addNextPosition(position);
            } else {
                addAttackOrProtect(piece);
            }
        }
    }

    /**
     * 获取到当前棋子能采取的所有动作
     */
    public final List<Action> getAllActions() {
        return getActions(null);
    }

    /**
     * 获取到当前棋子能采取的、能满足指定条件的动作
     */
    public final List<Action> getActions(@Nullable Predicate<Position> nextPositionFilter) {
        List<Action> list = new ArrayList<>(nextPositions.size());
        for (Position nextPosition : nextPositions) {
            if (nextPositionFilter == null || nextPositionFilter.test(nextPosition)) {
                list.add(new Action(position, nextPosition));
            }
        }
        return list;
    }


    //
    // ====================== 核心方法 ======================
    //

    /**
     * 刷新信息；在初始化或棋盘上的棋子发生变动时，会调用该方法更新信息
     *
     * @param positions 如果不为空数组，说明这些位置上有棋子新增、删除或替换；否则无条件刷新
     */
    public final void refresh(@NonNull Position[] positions) {
        if (positions.length == 0 || anyInRange(positions)) {
            clear();
            scan();
        }
    }

    /**
     * 判断当前棋子的信息是否会受positions中的至少一个位置的影响；需要子类{@link #isInRange 自己判断}
     */
    private boolean anyInRange(Position[] positions) {
        for (Position position : positions) {
            if (this.position == position || isInRange(position)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 清空所有信息；子类如果自己定义了一些状态信息，则可以重写本方法，把自己的状态信息也清除掉
     */
    public void clear() {
        for (Piece attack : attacks) {
            attack.attackers.remove(this);
        }
        attacks.clear();

        for (Piece protect : protects) {
            protect.protectors.remove(this);
        }
        protects.clear();

        nextPositions.clear();
    }

    /**
     * 真正进行扫描；这里需要子类自己实现{@link #doScanAt(Position, PositionHandler) 扫描的逻辑}
     */
    protected void scan() {
        doScanAt(position, p -> {
            addIfValid(p);
            return false;
        });
    }

    /**
     * 想办法让本棋子攻击或保护到指定的棋子（已知当前没法攻击或保护到目标）
     */
    public List<Action> getAttackOrProtectActions(Piece target) {

        // 默认采用简单模拟的方式：先尝试走一步，然后判断走这一步后是否能够到目标棋子
        // 注意，除了简单模拟之外，可能还可以通过移动别的棋子的方式（如松开马腿）来让当前棋子攻击或保护到目标棋子
        List<Action> list = new ArrayList<>();
        for (Position nextPosition : nextPositions) {
            if (doScanAt(nextPosition, p -> p == target.position)) {
                list.add(new Action(position, nextPosition));
            }
        }
        return list;
    }

    /**
     * 坐标处理器
     */
    protected interface PositionHandler {

        /**
         * 每扫描到一个坐标，就会调用一次本方法；如果返回true，则提前停止扫描
         */
        boolean handle(@Nullable Position position);
    }

    /**
     * 判断指定位置是否在当前棋子的攻击范围内，或会影响到该棋子的移动；如果是，则会触发刷新操作
     */
    protected abstract boolean isInRange(Position target);

    /**
     * 从指定位置开始扫描所有可到达的位置，并通过处理器来处理这些位置；返回值代表扫描是否提前停止
     */
    protected abstract boolean doScanAt(Position position, PositionHandler handler);
}