package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Color;
import com.laidw.chess.common.Direction;
import com.laidw.chess.common.Position;
import com.laidw.chess.common.Role;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 棋子：兵
 *
 * @author NightDW 2023/10/6 19:16
 */
public class SoldierPiece extends OneStepPiece {
    private static final List<List<Direction>> redIntendedDirections = Arrays.asList(
            Arrays.asList(Direction.getForward(Color.RED), Direction.LEFT, Direction.RIGHT),
            Collections.singletonList(Direction.getForward(Color.RED))
    );

    private static final List<List<Direction>> blackIntendedDirections = Arrays.asList(
            Arrays.asList(Direction.getForward(Color.BLACK), Direction.LEFT, Direction.RIGHT),
            Collections.singletonList(Direction.getForward(Color.BLACK))
    );

    public SoldierPiece(Color color, Position position, Chess chess) {
        super(color, Role.SOLDIER, position, chess);
    }

    @Override
    protected boolean isInRange(Position target) {

        // 除了需要距离为1之外，还需要确保从当前位置能走到目标位置
        return super.isInRange(target)
                && getIntendedDirections(position).contains(Direction.getNormal(position, target));
    }

    @Override
    protected List<Direction> getIntendedDirections(Position position) {

        // 如果已过河，则可以往前、左、右；否则，只能往前
        List<List<Direction>> intendedDirections = color == Color.RED ? redIntendedDirections : blackIntendedDirections;
        return position.toRelative(color).isInSide() ? intendedDirections.get(1) : intendedDirections.get(0);
    }
}