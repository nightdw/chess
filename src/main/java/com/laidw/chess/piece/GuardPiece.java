package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.*;

import java.util.Collections;
import java.util.List;

/**
 * 棋子：士
 *
 * @author NightDW 2023/10/6 23:26
 */
public class GuardPiece extends Piece {
    public GuardPiece(Color color, Position position, Chess chess) {
        super(color, Role.GUARD, position, chess);
    }

    @Override
    protected boolean isInRange(Position target) {

        // 变动的位置必须在皇宫内，且必须是皇宫的四角或中心，并且两者正好都落在边长为1的正方形中
        Position.Relative relative = target.toRelative(color);
        return relative.isInPalace()
                && (relative.x == 1) == (relative.y == 4)
                && (Math.abs(position.x - target.x) == 1 && Math.abs(position.y - target.y) == 1);
    }

    @Override
    protected boolean doScanAt(Position position, PositionHandler handler) {
        for (Direction direction : Direction.obliques()) {
            Position next = direction.next(position);
            if (next != null && next.toRelative(color).isInPalace() && handler.handle(next)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Action> getAttackOrProtectActions(Piece target) {

        // 如果对方在皇宫内，且是皇宫的四角或中心，那么，只要本棋子能移到皇宫中心，则一定能攻击或保护到目标
        Position.Relative relative = target.position.toRelative(color);
        if (relative.isInPalace() && (relative.x == 1) == (relative.y == 4)) {
            Position center = new Position.Relative(1, 4).toAbsolute(color);
            if (nextPositions.contains(center)) {
                return Collections.singletonList(new Action(position, center));
            }
        }
        return Collections.emptyList();
    }
}
