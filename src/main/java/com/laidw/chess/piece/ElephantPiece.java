package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.*;

import java.util.Collections;
import java.util.List;

/**
 * 棋子：象
 *
 * @author NightDW 2023/10/6 23:25
 */
public class ElephantPiece extends Piece {
    public ElephantPiece(Color color, Position position, Chess chess) {
        super(color, Role.ELEPHANT, position, chess);
    }

    @Override
    protected boolean isInRange(Position target) {

        // 变动的位置必须在我方境内，且必须是象脚或别象腿的位置，并且两者在边长为2的正方形中
        Position.Relative relative = target.toRelative(color);
        return relative.isInSide()
                && isReachableOrBlocking(relative)
                && (Math.abs(position.x - target.x) <= 2 && Math.abs(position.y - target.y) <= 2);
    }

    @Override
    protected boolean doScanAt(Position position, PositionHandler handler) {
        for (Direction direction : Direction.obliques()) {
            Position blockPosition = direction.next(position);
            if (blockPosition != null && chess.get(blockPosition) == null) {
                Position next = direction.next(blockPosition);
                if (next != null && next.toRelative(color).isInSide() && handler.handle(next)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<Action> getAttackOrProtectActions(Piece target) {
        Position.Relative relative = target.position.toRelative(color);

        // 必须确保目标位于我方的象脚点上，并且当前棋子与目标的距离等于4
        if (relative.isInSide() && isReachableOrBlocking(relative) && (relative.x & 2) == 0 && position.getDistanceWith(target.position) == 4) {

            // 如果位于同一行或同一列，则用模拟法解决
            if (position.x - target.position.x == 0 || position.y - target.position.y == 0) {
                return super.getAttackOrProtectActions(target);
            }

            // 否则，考虑通过松开象腿来解决
            Piece fulcrum = chess.get(Position.avg(position, target.position));
            assert fulcrum != null;
            return isOpponent(fulcrum) ? Collections.emptyList() : fulcrum.getAllActions();
        }
        return Collections.emptyList();
    }

    private boolean isReachableOrBlocking(Position.Relative relative) {
        int x = relative.x, y = relative.y;
        if ((x & 1) == 1) {
            return (y & 1) == 1;
        }
        int rx = x & 3, ry = y & 3;
        return (rx == 0 && ry == 2) || (rx == 2 && ry == 0);
    }
}
