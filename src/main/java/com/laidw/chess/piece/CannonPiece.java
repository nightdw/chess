package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.*;
import com.laidw.chess.util.ChessUtil;
import com.laidw.chess.util.PieceUtil;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * 棋子：炮
 *
 * @author NightDW 2023/10/6 23:22
 */
public class CannonPiece extends LineBasedPiece {
    public CannonPiece(Color color, Position position, Chess chess) {
        super(color, Role.CANNON, position, chess);
    }

    @Override
    protected void doScanFrom(Position position, Direction direction, @Nullable Consumer<Position> blankHandler, Consumer<Piece> pieceConsumer) {
        Piece fulcrum = ChessUtil.scanBlank(chess, position, direction, blankHandler);
        if (fulcrum != null) {
            Piece target = ChessUtil.scanBlank(chess, fulcrum.position, direction, null);
            if (target != null) {
                pieceConsumer.accept(target);
            }
        }
    }

    @Override
    protected List<Action> getActionsByFulcrums(List<Piece> fulcrums, Piece piece) {

        // 对于炮来说，此时的fulcrums集合的元素个数不可能是1
        // 如果没有元素，即空头炮，则随便将一个我方棋子移动到炮和目标之间即可；另外，还可以把炮往后打
        // 如果有2个元素，则将其中的我方棋子移出炮和目标之间即可；或者用其中的我方棋子吃掉对方的棋子
        // 如果有3个元素，则第2个元素必须是敌方的，此时可以打掉该棋子而攻击或保护到目标

        if (fulcrums.size() == 3) {
            return isOpponent(fulcrums.get(1)) ? Collections.singletonList(new Action(position, fulcrums.get(1).position)) : Collections.emptyList();
        }

        if (fulcrums.size() == 2) {
            List<Action> list = new ArrayList<>();
            int friendCount = 0, friendIdx = -1;
            for (int i = 0; i < 2; i++) {
                Piece fulcrum = fulcrums.get(i);
                if (!isOpponent(fulcrum)) {
                    friendCount++;
                    friendIdx = i;
                    PieceUtil.addLeaveLineActions(position, piece.position, fulcrum, list);
                }
            }
            if (friendCount == 1 && fulcrums.get(friendIdx).canMoveTo(fulcrums.get(1 - friendIdx).position)) {
                list.add(new Action(fulcrums.get(friendIdx).position, fulcrums.get(1 - friendIdx).position));
            }
            return list;
        }

        if (fulcrums.size() == 0) {
            List<Action> list = new ArrayList<>();
            Predicate<Position> filter = PieceUtil.getBetweenFilter(position, piece.position);

            // 将我方棋子移动到炮和目标之间
            // 需要过滤掉当前棋子和目标棋子，并且如果要移动的棋子是将，那么目标棋子不能是将
            for (Piece friend : chess.getAllPieces(color)) {
                if (friend != this && friend != piece && (friend.role != Role.KING || piece.role != Role.KING)) {
                    list.addAll(friend.getActions(filter));
                }
            }

            // 判断是否能往后打，如果能，则添加该动作
            doScanFrom(position, Direction.getNormal(piece.position, position), null, p -> {
                if (isOpponent(p)) {
                    list.add(new Action(position, p.position));
                }
            });

            return list;
        }

        return Collections.emptyList();
    }
}
