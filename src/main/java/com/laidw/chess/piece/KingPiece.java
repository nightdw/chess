package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.*;
import com.laidw.chess.util.ChessUtil;

import java.util.*;

/**
 * 棋子：帅
 *
 * @author NightDW 2023/10/6 23:26
 */
public class KingPiece extends OneStepPiece {
    private static final Map<Position, List<Direction>> intendedDirectionsMap = new HashMap<>();
    static {
        for (int x = 0; x < 3; x++) {
            for (int y = 3; y < 6; y++) {
                List<Direction> directions = new ArrayList<>(4);
                if (y > 3) {
                    directions.add(Direction.LEFT);
                }
                if (y < 5) {
                    directions.add(Direction.RIGHT);
                }
                if (x > 0) {
                    directions.add(Direction.UP);
                }
                if (x < 2) {
                    directions.add(Direction.DOWN);
                }
                intendedDirectionsMap.put(Position.of(x, y), Collections.unmodifiableList(directions));
            }
        }
    }

    public KingPiece(Color color, Position position, Chess chess) {
        super(color, Role.KING, position, chess);
    }

    @Override
    protected boolean isInRange(Position target) {

        // 当变动的位置在将帅的上下左右4格，且在我方皇宫之内时，需要扫描
        // 特殊情况：将帅之间面对面时可以相互攻击，并且将帅可以给我方棋子加根，因此，当变动的位置和当前棋子在同一列时，也需要扫描
        return (super.isInRange(target) && target.toRelative(color).isInPalace())
                || position.y == target.y;
    }

    @Override
    protected boolean doScanAt(Position position, PositionHandler handler) {
        if (super.doScanAt(position, handler)) {
            return true;
        }

        // 继续沿着当前列往前扫描；如果得到的棋子不在我方皇宫内，且要么是我方棋子，要么是对方的将，则又找到一个
        Piece target = ChessUtil.scanBlank(chess, position, Direction.getForward(color), null);
        if (target != null && !target.position.toRelative(color).isInPalace() && (!isOpponent(target) || target.getRole() == Role.KING)) {
            return handler.handle(target.position);
        }
        return false;
    }

    @Override
    protected List<Direction> getIntendedDirections(Position position) {

        // 这里可以把红帅当作黑将处理，直接把红帅的x坐标减去7即可
        return intendedDirectionsMap.get(color == Color.BLACK ? position : Position.of(position.x - 7, position.y));
    }

    @Override
    public List<Action> getAttackOrProtectActions(Piece target) {

        // 只有当目标在我方皇宫中时，才有必要扫描
        return target.position.toRelative(color).isInPalace() ?
                super.getAttackOrProtectActions(target) :
                Collections.emptyList();
    }
}
