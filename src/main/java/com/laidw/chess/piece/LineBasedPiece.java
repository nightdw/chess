package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.*;
import com.laidw.chess.util.ChessUtil;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * 能走一整行/列的棋子，如车、炮
 *
 * @author NightDW 2023/10/6 19:16
 */
public abstract class LineBasedPiece extends Piece {

    /**
     * 记录上下左右4个方向最远能到达的坐标
     */
    protected final Map<Direction, Position> endPositionMap = new HashMap<>(4);

    protected LineBasedPiece(Color color, Role role, Position position, Chess chess) {
        super(color, role, position, chess);
    }

    /**
     * 本类会重写{@link #scan()}和{@link #getAttackOrProtectActions(Piece) getAttackOrProtectActions()}方法，因此不需要实现该方法了
     */
    @Override
    protected final boolean doScanAt(Position position, PositionHandler handler) {
        throw new UnsupportedOperationException();
    }

    /**
     * 清除信息时，还需要将endPositionMap集合清空
     */
    @Override
    public final void clear() {
        super.clear();
        endPositionMap.clear();
    }

    /**
     * 在扫描时，还需要将一些额外信息添加到endPositionMap集合中；需要子类实现{@link #doScanFrom(Position, Direction, Consumer, Consumer) doScanFrom()}方法
     */
    @Override
    protected final void scan() {
        for (Direction direction : Direction.normals()) {

            // 沿着当前方向进行扫描
            // 将扫描到的空格添加到nextPositions集合中
            // 将扫描到的棋子添加到相应的集合中，并将其坐标当作是该方向上的最远坐标
            doScanFrom(position, direction, this::addNextPosition, piece -> {
                addAttackOrProtect(piece);
                endPositionMap.put(direction, piece.position);
            });
        }
    }

    @Override
    protected final boolean isInRange(Position target) {

        // 变动的位置必须和该棋子位于同一行/列
        // 并且变动的位置不能超过最远的地方（因为超出的那部分对本棋子来说是不可见的）
        return (this.position.x == target.x || this.position.y == target.y)
                && isNotOutOfEndPosition(target);
    }

    private boolean isNotOutOfEndPosition(Position target) {
        Direction direction = Direction.getNormal(position, target);
        Position endPosition = endPositionMap.get(direction);

        // 当没有终点（终点为无限），或目标位置是终点，或目标位置到终点的方向等于当前位置到终点的方向时，说明确实没超出
        return endPosition == null
                || endPosition == target
                || Direction.getNormal(target, endPosition) == direction;
    }

    @Override
    public final List<Action> getAttackOrProtectActions(Piece target) {
        Position targetPosition = target.position;

        // 如果和目标在同一行/列，则说明我方的车/炮与目标之间有若干个棋子阻拦
        // 此时，找到阻拦的这些棋子，然后由子类根据这些棋子信息来获取可行解
        if (this.position.x == targetPosition.x || this.position.y == targetPosition.y) {
            Direction direction = Direction.getNormal(position, targetPosition);
            List<Piece> fulcrums = new ArrayList<>();
            Position cur = position;
            while (true) {
                Piece fulcrum = ChessUtil.scanBlank(chess, cur, direction, null);
                if (fulcrum == null || fulcrum.position == targetPosition) {
                    break;
                }
                fulcrums.add(fulcrum);
                cur = fulcrum.position;
            }
            return getActionsByFulcrums(fulcrums, target);
        }

        // 否则，说明本棋子和目标在一个矩形的两个对角上，此时可能攻击或保护到目标的只有矩形的另外两个对角
        List<Action> list = new ArrayList<>(2);
        for (Position candidate : new Position[] {Position.of(position.x, targetPosition.y), Position.of(targetPosition.x, position.y)}) {

            // 如果确实能到达矩形的对角
            if (canMoveTo(candidate)) {

                // 沿着该方向扫描棋子；如果扫描到的棋子就是目标棋子，说明能攻击或保护到目标棋子
                doScanFrom(candidate, Direction.getNormal(candidate, targetPosition), null, piece -> {
                    if (piece == target) {
                        list.add(new Action(position, candidate));
                    }
                });
            }
        }
        return list;
    }

    /**
     * 从指定位置开始，沿指定方向进行扫描；扫描到的空格和能攻击或保护到的棋子将会被对应的处理器处理
     */
    protected abstract void doScanFrom(Position position, Direction direction, @Nullable Consumer<Position> blankHandler, Consumer<Piece> pieceConsumer);

    /**
     * 已知本棋子和目标之间有若干个棋子，根据这些信息获取能够攻击或保护到目标的动作
     */
    protected abstract List<Action> getActionsByFulcrums(List<Piece> fulcrums, Piece piece);
}
