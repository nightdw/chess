package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.*;

import java.util.Collections;
import java.util.List;

/**
 * 只能在上/下/左/右方向行走一格的棋子，如兵、帅
 *
 * @author NightDW 2023/10/6 18:40
 */
public abstract class OneStepPiece extends Piece {
    protected OneStepPiece(Color color, Role role, Position position, Chess chess) {
        super(color, role, position, chess);
    }

    @Override
    protected boolean isInRange(Position target) {

        // 变动的位置必须是该棋子的上下左右4格；子类可能还需要再进一步判断
        return position.getDistanceWith(target) == 1;
    }

    @Override
    protected boolean doScanAt(Position position, PositionHandler handler) {
        for (Direction direction : getIntendedDirections(position)) {
            if (handler.handle(direction.next(position))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Action> getAttackOrProtectActions(Piece target) {

        // 当与目标距离为2时，才需要尝试获取；子类可能还需要再进一步判断
        return position.getDistanceWith(target.position) == 2 ?
                super.getAttackOrProtectActions(target) :
                Collections.emptyList();
    }

    /**
     * 获取本棋子在指定位置上能走的方向；允许有越界方向（比如当前已经在最右侧了，此时可以有右方向）
     */
    protected abstract List<Direction> getIntendedDirections(Position position);
}
