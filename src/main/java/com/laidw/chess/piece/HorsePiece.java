package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.*;

import java.util.*;

/**
 * 棋子：马
 *
 * @author NightDW 2023/10/6 23:24
 */
public class HorsePiece extends Piece {
    private static final Map<Direction, List<Direction>> horseDirectionMap = new HashMap<>();
    static {
        horseDirectionMap.put(Direction.UP, Arrays.asList(Direction.UP_LEFT, Direction.UP_RIGHT));
        horseDirectionMap.put(Direction.DOWN, Arrays.asList(Direction.DOWN_LEFT, Direction.DOWN_RIGHT));
        horseDirectionMap.put(Direction.LEFT, Arrays.asList(Direction.UP_LEFT, Direction.DOWN_LEFT));
        horseDirectionMap.put(Direction.RIGHT, Arrays.asList(Direction.UP_RIGHT, Direction.DOWN_RIGHT));
    }

    public HorsePiece(Color color, Position position, Chess chess) {
        super(color, Role.HORSE, position, chess);
    }

    @Override
    protected boolean isInRange(Position target) {
        int absX = Math.abs(position.x - target.x);
        int absY = Math.abs(position.y - target.y);
        int absSum = absX + absY;

        // 当absSum为1时，变动的位置正好在马的上下左右4个格子上
        // 当absSum为3，且absX和absY都不为0时，变动的位置正好在马脚上
        return absSum == 1 || (absSum == 3 && (absX == 1 || absY == 1));
    }

    @Override
    protected boolean doScanAt(Position position, PositionHandler handler) {
        for (Map.Entry<Direction, List<Direction>> entry : horseDirectionMap.entrySet()) {
            Position blockPosition = entry.getKey().next(position);
            if (blockPosition != null && chess.get(blockPosition) == null) {
                for (Direction nextDirection : entry.getValue()) {
                    if (handler.handle(nextDirection.next(blockPosition))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public List<Action> getAttackOrProtectActions(Piece target) {
        int absX = Math.abs(position.x - target.position.x);
        int absY = Math.abs(position.y - target.position.y);
        int distance = absX + absY;

        // 如果目标正好在马脚上，则考虑通过松开马腿来解决
        if (distance == 3 && (absX == 1 || absY == 1)) {
            Piece fulcrum = chess.get(Position.horseFulcrum(position, target.position));
            assert fulcrum != null;
            return isOpponent(fulcrum) ? Collections.emptyList() : fulcrum.getAllActions();
        }

        // 当距离为1、3，5，或大于6时，肯定无法通过两步到达目标点；否则还是采用模拟法解决
        return distance > 6 || (distance & 1) == 1 ? Collections.emptyList() : super.getAttackOrProtectActions(target);
    }
}
