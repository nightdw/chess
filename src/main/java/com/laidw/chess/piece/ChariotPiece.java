package com.laidw.chess.piece;

import com.laidw.chess.Chess;
import com.laidw.chess.common.*;
import com.laidw.chess.util.ChessUtil;
import com.laidw.chess.util.PieceUtil;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * 棋子：车
 *
 * @author NightDW 2023/10/6 23:23
 */
public class ChariotPiece extends LineBasedPiece {
    public ChariotPiece(Color color, Position position, Chess chess) {
        super(color, Role.CHARIOT, position, chess);
    }

    @Override
    protected void doScanFrom(Position position, Direction direction, @Nullable Consumer<Position> blankHandler, Consumer<Piece> pieceConsumer) {
        Piece piece = ChessUtil.scanBlank(chess, position, direction, blankHandler);
        if (piece != null) {
            pieceConsumer.accept(piece);
        }
    }

    @Override
    protected List<Action> getActionsByFulcrums(List<Piece> fulcrums, Piece piece) {

        // 对于车来说，此时fulcrums集合一定至少有1个元素
        // 并且，车和目标之间只能有1个棋子，否则肯定不能攻击或保护到目标
        if (fulcrums.size() != 1) {
            return Collections.emptyList();
        }

        // 如果中间棋子是对方的，则吃掉它
        Piece fulcrum = fulcrums.get(0);
        if (isOpponent(fulcrum)) {
            return Collections.singletonList(new Action(position, fulcrum.position));
        }

        // 否则，将中间棋子移动到别的行/列即可
        List<Action> list = new ArrayList<>();
        PieceUtil.addLeaveLineActions(position, piece.position, fulcrum, list);
        return list;
    }
}
