package com.laidw.chess.controller;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.common.Color;
import com.laidw.chess.common.Position;
import com.laidw.chess.common.Role;
import com.laidw.chess.util.VccUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * 解残局功能的控制器；残局信息存放在Session中
 *
 * @author NightDW 2023/11/28 17:51
 */
@Controller
@RequestMapping("/ending")
public class EndingController {
    private static final String VIEW_NAME = "ending";
    private static final String CHESS_KEY = "chess";

    /**
     * 残局快照文件的路径
     */
    @Value("${snapshot.location}")
    private String snapshotLocation;

    /**
     * 用于加载残局快照文件
     */
    @Autowired
    private ResourceLoader resourceLoader;

    /**
     * 首页；如果指定了useDefaultLayout参数，则重置残局信息，否则读取已有的残局信息（没有则自动创建）
     */
    @GetMapping({"/", "index"})
    public ModelAndView index(HttpServletRequest request, Boolean useDefaultLayout) {
        Chess chess;
        if (useDefaultLayout == null) {
            chess = getChess(request);
            if (chess == null) {
                setChess(request, chess = new Chess());
            }
        } else {
            chess = new Chess(useDefaultLayout);
            setChess(request, chess);
        }
        return newMavWithEndingChess(chess);
    }

    /**
     * 往棋盘上摆放棋子
     */
    @GetMapping("/put")
    public ModelAndView put(HttpServletRequest request, int x, int y, Color color, Role role) {
        Chess chess = getRequireChess(request);
        chess.put(Position.of(x, y), color, role);
        return newMavWithEndingChess(chess);
    }

    /**
     * 删除棋盘上的棋子
     */
    @GetMapping("/remove")
    public ModelAndView remove(HttpServletRequest request, int x, int y) {
        Chess chess = getRequireChess(request);
        chess.remove(Position.of(x, y));
        return newMavWithEndingChess(chess);
    }

    /**
     * 移动棋子
     */
    @GetMapping("/move")
    public ModelAndView move(HttpServletRequest request, int fx, int fy, int tx, int ty) {
        Chess chess = getRequireChess(request);
        chess.move(Position.of(fx, fy), Position.of(tx, ty));
        return newMavWithEndingChess(chess);
    }

    /**
     * 撤回
     */
    @GetMapping("/back")
    public ModelAndView back(HttpServletRequest request) {
        Chess chess = getRequireChess(request);
        chess.back();
        return newMavWithEndingChess(chess);
    }

    /**
     * 使用VCC算法自动推断下一步应该怎么走；如果推断不出，则不做任何事
     */
    @GetMapping("/next")
    public ModelAndView next(HttpServletRequest request, Color color) {
        Chess chess = getRequireChess(request);
        Action action = VccUtil.tryVcc(chess, color);
        if (action != null) {
            chess.move(action.from, action.to);
        }
        return newMavWithEndingChess(chess);
    }

    /**
     * 将当前局面保存到快照文件中
     */
    @GetMapping("/save")
    public ModelAndView save(HttpServletRequest request) throws IOException {
        Chess chess = getRequireChess(request);
        String snapshot = chess.getSnapshot().toString() + "\n";
        try (RandomAccessFile rw = new RandomAccessFile(resourceLoader.getResource(snapshotLocation).getFile(), "rw")) {
            rw.seek(rw.length());
            rw.write(snapshot.getBytes(StandardCharsets.UTF_8));
        }
        return newMavWithEndingChess(chess);
    }

    /**
     * 加载残局快照文件中的某个残局信息
     */
    @GetMapping("/load")
    public ModelAndView load(HttpServletRequest request, int num) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(resourceLoader.getResource(snapshotLocation).getFile().toPath())));
        for (int i = 0; i < num; i++) {
            br.readLine();
        }
        Chess chess = new Chess.Snapshot(br.readLine()).resume();
        setChess(request, chess);
        return newMavWithEndingChess(chess);
    }

    @Nullable
    private static Chess getChess(HttpServletRequest request) {
        return (Chess) request.getSession().getAttribute(CHESS_KEY);
    }

    private static Chess getRequireChess(HttpServletRequest request) {
        Chess chess = getChess(request);
        if (chess == null) {
            throw new RuntimeException("不存在残局信息");
        }
        return chess;
    }

    private static void setChess(HttpServletRequest request, Chess chess) {
        request.getSession().setAttribute(CHESS_KEY, chess);
    }

    private static ModelAndView newMavWithEndingChess(Chess chess) {
        ModelAndView mav = new ModelAndView(VIEW_NAME);
        mav.addObject(CHESS_KEY, chess);
        return mav;
    }
}
