package com.laidw.chess.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.laidw.chess.Chess;
import com.laidw.chess.battle.Battle;
import com.laidw.chess.battle.match.MatchStrategy;
import com.laidw.chess.common.Color;
import com.laidw.chess.common.Position;
import com.laidw.chess.entity.User;
import com.laidw.chess.piece.Piece;
import com.laidw.chess.service.UserService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.laidw.chess.controller.BattleConfig.BATTLE_REGISTRY;
import static com.laidw.chess.controller.BattleConfig.MATCHER;

/**
 * 与对战界面的WebSocket进行交互
 *
 * @author NightDW 2023/12/1 18:29
 */
@Component
@ServerEndpoint("/battle/{userId}")
public class BattleWsEndpoint {

    /**
     * 保存用户与会话的关联关系；一个用户只能开启一个会话
     */
    private static final Map<Long, Session> USER_ID_TO_SESSION = new ConcurrentHashMap<>();

    /**
     * 当同一个用户开启多个WebSocket时，多余的WebSocket需要自动关闭，本对象就代表此时的关闭原因
     */
    private static final CloseReason DUPLICATE_SESSION = new CloseReason(() -> 0, "重复创建会话");

    /**
     * 对象转JSON的工具
     */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static UserService userService;

    /**
     * 真正工作的本类对象并不是由Spring创建的，因此Spring中的Bean无法直接注入到本类对象中
     * 但Spring容器在初始化时还是会创建一次本类的Bean，我们可以借此把Spring中的Bean保存到静态变量中
     */
    @Autowired
    public void setUserService(UserService userService) {
        BattleWsEndpoint.userService = userService;
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("userId") Long userId) throws IOException {
        Session oldSession = USER_ID_TO_SESSION.putIfAbsent(userId, session);
        if (oldSession != null) {
            send(session, Response.error("一个用户只能开启一个WebSocket"));
            session.close(DUPLICATE_SESSION);
        } else {
            User user = User.ofUserId(userId);
            Battle battle = BATTLE_REGISTRY.getBattle(user);
            MatchStrategy strategy;
            if (battle != null) {
                send(session, Response.battle(battle, user));
            } else if ((strategy = MATCHER.getMatchStrategy(user)) != null) {
                send(session, Response.matching(strategy));
            } else {
                send(session, Response.match());
            }
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason, @PathParam("userId") Long userId) throws IOException {
        if (closeReason == DUPLICATE_SESSION) {
            return;
        }
        try (Session s = USER_ID_TO_SESSION.remove(userId)) {
            if (s != session) {
                throw new IllegalStateException("用户id与会话不匹配");
            }
        }
    }

    @OnMessage
    public void onMessage(Session session, String message, @PathParam("userId") Long userId) {
        String[] split = message.split(",");
        Response response;
        try {
            response = CommandHandler.get(split[0]).handle(userService.get(userId), session, split);
        } catch (Exception e) {
            response = Response.error(e.getMessage());
        }
        send(session, response);
    }

    private static void send(Session session, Response response) {
        try {
            session.getBasicRemote().sendText(OBJECT_MAPPER.writeValueAsString(response));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Getter
    private static class Response {
        private final String scene;
        private final Object data;
        private Response(String scene, Object data) {
            this.scene = scene;
            this.data = data;
        }

        public static Response empty() {
            return new Response("", null);
        }

        public static Response info(String message) {
            return new Response("info", message);
        }

        public static Response error(String message) {
            return new Response("error", message);
        }

        public static Response match() {
            return new Response("match", null);
        }

        public static Response matching(MatchStrategy strategy) {
            return new Response("matching", strategy.name());
        }

        public static Response battle(Battle battle, User user) {
            User opponent = (User) battle.getOpponentOf(user);
            return new Response("battle", new BattleVO(toFriendlyView(battle, user), battle.getResult().value, opponent.getUsername(), opponent.getScore()));
        }

        public static Response confirm(String type) {
            return new Response("confirm", type);
        }

        private static PieceVO[][] toFriendlyView(Battle battle, User user) {
            Chess chess = battle.getChess();
            Piece[][] board = chess.getBoard();
            int m = board.length;
            int n = board[0].length;

            PieceVO[][] voBoard = new PieceVO[m][];
            for (int i = 0; i < m; i++) {
                PieceVO[] voLine = new PieceVO[n];
                for (int j = 0; j < n; j++) {
                    voLine[j] = new PieceVO(chess, Position.of(i, j));
                }
                voBoard[i] = voLine;
            }

            // 如果是黑方，则需要旋转180度
            if (battle.getColorOf(user) == Color.BLACK) {
                for (int i = 0; i < m / 2; i++) {
                    for (int j = 0; j < n; j++) {
                        PieceVO tem = voBoard[i][j];
                        voBoard[i][j] = voBoard[m - 1 - i][n - 1 - j];
                        voBoard[m - 1 - i][n - 1 - j] = tem;
                    }
                }
            }

            return voBoard;
        }
    }

    @Getter
    private static class PieceVO {
        private final int color;
        private final String name;
        private final boolean last;
        private final int x, y;

        public PieceVO(Chess chess, Position position) {
            int x = position.x, y = position.y;
            this.x = x;
            this.y = y;
            this.last = chess.isLast(x, y);

            Piece piece = chess.get(position);
            if (piece != null) {
                this.color = piece.getColor().value;
                this.name = piece.getRole().getName(piece.getColor());
            } else {
                this.color = 2;
                this.name = "〇";
            }
        }
    }

    @Getter
    @AllArgsConstructor
    private static class BattleVO {
        private final PieceVO[][] board;
        private final int result;
        private final String opponentName;
        private final int opponentScore;
    }

    private enum CommandHandler {
        unknown {
            @Override
            public Response handle(User user, Session session, String[] args) {
                return Response.error("未知的命令");
            }
        },
        startMatch {
            @Override
            public Response handle(User user, Session session, String[] args) {
                MatchStrategy strategy = MatchStrategy.valueOf(args[1]);
                MATCHER.matchAsync(user, strategy)
                        .whenComplete((result, error) -> {
                            if (error != null) {
                                send(session, Response.error(error.getMessage()));
                            } else if (result != null) {
                                send(session,  Response.battle(BATTLE_REGISTRY.getOrCreateBattle(user, result), user));
                            }
                        });
                return Response.matching(strategy);
            }
        },
        stopMatch {
            @Override
            public Response handle(User user, Session session, String[] args) {
                MATCHER.cancel(user);
                return Response.match();
            }
        },
        move {
            @Override
            Response handle(User user, Session session, String[] args) {
                Battle battle = BATTLE_REGISTRY.getRequiredBattle(user);
                battle.move(user, Position.of(Integer.parseInt(args[1]), Integer.parseInt(args[2])), Position.of(Integer.parseInt(args[3]), Integer.parseInt(args[4])));

                if (battle.isFinished()) {
                    BATTLE_REGISTRY.remove(user);
                }

                User opponent = (User) battle.getOpponentOf(user);
                Session opponentSession = USER_ID_TO_SESSION.get(opponent.getUserId());
                if (opponentSession != null) {
                    send(opponentSession, Response.battle(battle, opponent));
                }
                return Response.battle(battle, user);
            }
        },
        require {
            @Override
            Response handle(User user, Session session, String[] args) {
                Battle battle = BATTLE_REGISTRY.getRequiredBattle(user);

                String type = args[1];
                if (type.equals("draw")) {
                    battle.requireDraw(user);
                } else {
                    battle.requireBack(user);
                }

                User opponent = (User) battle.getOpponentOf(user);
                Session opponentSession = USER_ID_TO_SESSION.get(opponent.getUserId());
                if (opponentSession != null) {
                    send(opponentSession, Response.confirm(type));
                    return Response.info("请求已发送");
                }
                return Response.error("找不到对手的会话");
            }
        },
        agree {
            @Override
            Response handle(User user, Session session, String[] args) {
                Battle battle = BATTLE_REGISTRY.getRequiredBattle(user);

                String type = args[1];
                boolean agree = Boolean.parseBoolean(args[2]);
                if (type.equals("draw")) {
                    battle.agreeDraw(user, agree);
                } else {
                    battle.agreeBack(user, agree);
                }

                User opponent = (User) battle.getOpponentOf(user);
                Session opponentSession = USER_ID_TO_SESSION.get(opponent.getUserId());
                if (opponentSession == null) {
                    return Response.error("找不到对手的会话");
                }

                // 如果我方不同意，则将该决定告诉对方，而我方无需任何处理
                if (!agree) {
                    send(opponentSession, Response.info("对方拒绝了您的请求"));
                    return Response.empty();
                }

                if (battle.isFinished()) {
                    BATTLE_REGISTRY.remove(user);
                }

                // 如果我方同意和棋或悔棋，则在告诉对方的同时，刷新双方的棋盘信息
                send(opponentSession, Response.info("对方同意了您的请求"));
                send(opponentSession, Response.battle(battle, opponent));
                return Response.battle(battle, user);
            }
        },
        giveUp {
            @Override
            Response handle(User user, Session session, String[] args) {
                Battle battle = BATTLE_REGISTRY.getRequiredBattle(user);
                BATTLE_REGISTRY.remove(user);
                battle.giveUp(user);

                User opponent = (User) battle.getOpponentOf(user);
                Session opponentSession = USER_ID_TO_SESSION.get(opponent.getUserId());
                if (opponentSession != null) {
                    send(opponentSession, Response.battle(battle, opponent));
                }
                return Response.battle(battle, user);
            }
        },
        ;

        abstract Response handle(User user, Session session, String[] args);

        public static CommandHandler get(String cmd) {
            for (CommandHandler value : values()) {
                if (value.name().equals(cmd)) {
                    return value;
                }
            }
            return unknown;
        }
    }
}
