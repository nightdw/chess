package com.laidw.chess.controller;

import com.laidw.chess.battle.BattleRegistry;
import com.laidw.chess.battle.match.Matcher;
import com.laidw.chess.battle.match.SimpleMatcher;
import com.laidw.chess.entity.User;

/**
 * 与对战相关的一些配置/组件
 *
 * @author NightDW 2023/12/2 13:27
 */
public interface BattleConfig {

    /**
     * 匹配大厅
     */
    Matcher<User> MATCHER = new SimpleMatcher<>();

    /**
     * 对战大厅；谁先开始匹配，谁就持红
     */
    BattleRegistry<User> BATTLE_REGISTRY = new BattleRegistry<>((p, m) -> m.isFirst());
}
