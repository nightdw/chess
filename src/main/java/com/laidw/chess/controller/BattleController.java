package com.laidw.chess.controller;

import com.laidw.chess.entity.User;
import com.laidw.chess.service.UserService;
import com.laidw.chess.util.BasicAuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 联机对战功能的控制器，需要登录访问
 *
 * @author NightDW 2023/12/1 16:43
 */
@Controller
@RequestMapping("/battle")
public class BattleController {
    private static final String VIEW_NAME = "battle";
    private static final String USER_KEY = "user";
    private static final String PORT_KEY = "port";

    @Autowired
    private UserService userService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping({"/", "index"})
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user = getUser(request);

        // 如果未登录，或尝试登录失败，则提醒用户登录
        if (user == null && (user = tryAuth(request)) == null) {
            BasicAuthUtil.sendNoAuthError(response);
            return null;
        }

        // 将用户id传递给视图，在创建WebSocket时需要回传该用户id
        // 同时将服务器的端口也传递过去，避免修改端口时需要改动页面源代码
        ModelAndView mav = new ModelAndView(VIEW_NAME);
        mav.addObject(USER_KEY, user);
        mav.addObject(PORT_KEY, serverPort);
        return mav;
    }

    private static void setUser(HttpServletRequest request, User user) {
        request.getSession().setAttribute(USER_KEY, user);
    }

    @Nullable
    private static User getUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(USER_KEY);
    }

    @Nullable
    private User tryAuth(HttpServletRequest request) {
        String[] auth = BasicAuthUtil.parseAuth(request);
        if (auth == null) {
            return null;
        }

        User user = userService.load(auth[0], auth[1]);
        if (user != null) {
            setUser(request, user);
        }
        return user;
    }
}
