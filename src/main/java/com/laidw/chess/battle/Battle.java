package com.laidw.chess.battle;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Color;
import com.laidw.chess.common.Position;
import com.laidw.chess.piece.Piece;
import com.laidw.chess.util.SolutionUtil;
import lombok.Getter;
import org.springframework.lang.Nullable;

/**
 * 玩家之间的对战
 *
 * @author NightDW 2023/11/30 0:34
 */
public class Battle {

    /**
     * 比赛结果；未结束、平局、红胜、黑胜
     */
    public enum Result {
        UNKNOWN(-1), DRAW(2), RED_WIN(Color.RED.value), BLACK_WIN(Color.BLACK.value);
        public final int value;
        Result(int value) {
            this.value = value;
        }
    }

    /**
     * 棋盘
     */
    @Getter
    private final Chess chess = new Chess();

    /**
     * 红黑双方玩家；players[0]代表红方，players[1]代表黑方
     */
    private final Player[] players;

    /**
     * 当前轮到哪一方行动；0代表红方，1代表黑方
     */
    private int turn = 0;

    /**
     * 比赛结果
     */
    private Result result = Result.UNKNOWN;

    /**
     * 提出和棋的玩家
     */
    private Player drawRequire = null;

    /**
     * 提出悔棋的玩家
     */
    private Player backRequire = null;

    public Battle(Player red, Player black) {
        players = new Player[] {red, black};
    }

    public Result getResult() {
        return result;
    }

    public boolean isFinished() {
        return result != Result.UNKNOWN;
    }

    /**
     * 获取指定玩家的颜色
     *
     * @param player 目标玩家
     */
    public Color getColorOf(Player player) {
        assertValidPlayer(player);
        return players[0].equals(player) ? Color.RED : Color.BLACK;
    }

    /**
     * 获取指定玩家的对手
     *
     * @param player 目标玩家
     */
    public Player getOpponentOf(Player player) {
        assertValidPlayer(player);
        return players[0].equals(player) ? players[1] : players[0];
    }

    /**
     * 玩家走棋
     *
     * @param player 走棋的玩家
     */
    public synchronized void move(Player player, Position from, Position to) {
        assertNotFinished();
        assertHasNotRequirement();
        assertInTurnAndValidPiece(player, chess.get(from));

        chess.move(from, to);
        Color me = getColorOf(player);

        // 如果移动之后我方被将，则不允许此次操作
        if (chess.isChecked(me)) {
            chess.back();
            throw new IllegalArgumentException("不能送将，或请先解将");
        }

        turn = 1 - turn;

        // 如果对方被将，并且无解，则我方获胜
        if (chess.isChecked(me.reverse()) && SolutionUtil.getSolutions(chess, me.reverse()).isEmpty()) {
            result = me == Color.RED ? Result.RED_WIN : Result.BLACK_WIN;
        }
    }

    /**
     * 请求和棋
     *
     * @param player 请求和棋的玩家
     */
    public synchronized void requireDraw(Player player) {
        assertValidPlayer(player);
        assertHasNotRequirement();
        drawRequire = player;
    }

    /**
     * 请求悔棋
     *
     * @param player 请求悔棋的玩家
     */
    public synchronized void requireBack(Player player) {
        assertValidPlayer(player);
        assertHasNotRequirement();

        // 不允许某一方在未走棋的情况下提出悔棋
        if (chess.getActionCount() < (player.equals(players[0]) ? 1 : 2)) {
            throw new IllegalStateException("未走过棋子，不能悔棋");
        }

        backRequire = player;
    }

    /**
     * 同意或拒绝和棋
     *
     * @param player 同意或拒绝和棋的玩家
     * @param agree  是否同意
     */
    public synchronized void agreeDraw(Player player, boolean agree) {
        assertValidPlayer(player);

        if (drawRequire == null || drawRequire.equals(player)) {
            throw new IllegalStateException("没有求和请求，或者不能处理自己的求和请求");
        }

        if (agree) {
            result = Result.DRAW;
        }
        drawRequire = null;
    }

    /**
     * 同意或拒绝悔棋
     *
     * @param player 同意或拒绝悔棋的玩家
     * @param agree  是否同意
     */
    public synchronized void agreeBack(Player player, boolean agree) {
        assertValidPlayer(player);

        if (backRequire == null || backRequire.equals(player)) {
            throw new IllegalStateException("没有悔棋请求，或者不能处理自己的悔棋请求");
        }

        // 当某一方提出悔棋时，如果另一方还没行动，则回退一次；否则需要连续回退两次
        // 因此，如果红方提出悔棋（即黑方同意悔棋），则应该将总步数回退到偶数；否则回退到奇数
        if (agree) {
            chess.back();
            turn = 1 - turn;
            if ((chess.getActionCount() & 1) != (player.equals(players[1]) ? 0 : 1)) {
                chess.back();
                turn = 1 - turn;
            }
        }

        backRequire = null;
    }

    /**
     * 玩家认输
     *
     * @param player 认输的玩家
     */
    public synchronized void giveUp(Player player) {
        assertValidPlayer(player);
        result = getColorOf(player) == Color.RED ? Result.BLACK_WIN : Result.RED_WIN;
    }

    private void assertNotFinished() {
        if (isFinished()) {
            throw new IllegalStateException("对局已结束");
        }
    }

    private void assertHasNotRequirement() {
        if (drawRequire != null || backRequire != null) {
            throw new IllegalStateException("当前存在求和请求或悔棋请求，请先处理");
        }
    }

    private void assertValidPlayer(Player player) {
        if (!player.equals(players[0]) && !player.equals(players[1])) {
            throw new IllegalArgumentException("玩家不存在");
        }
    }

    private void assertInTurnAndValidPiece(Player player, @Nullable Piece piece) {
        if (!players[turn].equals(player)) {
            throw new IllegalStateException("未轮到当前玩家");
        }
        if (piece == null || piece.getColor().value != turn) {
            throw new IllegalArgumentException("请移动本方棋子");
        }
    }
}
