package com.laidw.chess.battle;

import com.laidw.chess.battle.match.Matcher;

/**
 * 决定对战双方谁持红方，谁持黑方
 *
 * @author NightDW 2023/11/30 20:01
 */
public interface ColorDeducer<P extends Player> {

    /**
     * 判断指定玩家是否应该持红；该方法应该具有对称性
     *
     * @param player      目标玩家
     * @param matchResult 该玩家的匹配结果
     */
    boolean isRed(P player, Matcher.Result<P> matchResult);
}
