package com.laidw.chess.battle.match;

import com.laidw.chess.battle.Player;

/**
 * 匹配策略
 *
 * @author NightDW 2023/11/7 15:42
 */
public enum MatchStrategy {
    ANY("任意") {
        @Override
        public boolean matches(Player p1, Player p2) {
            return true;
        }
    },
    SCORE_10("积分差不超过10") {
        @Override
        public boolean matches(Player p1, Player p2) {
            return Math.abs(p1.getScore() - p2.getScore()) <= 10;
        }
    },
    ;

    public final String displayName;
    MatchStrategy(String displayName) {
        this.displayName = displayName;
    }

    /**
     * 判断在本策略下，第二个玩家是否满足第一个玩家的要求；单向判断
     *
     * @param p1 第一个玩家
     * @param p2 第二个玩家
     * @return   第二个玩家是否满足第一个玩家的要求
     */
    public abstract boolean matches(Player p1, Player p2);
}
