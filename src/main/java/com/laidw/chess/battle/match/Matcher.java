package com.laidw.chess.battle.match;

import com.laidw.chess.battle.Player;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.Nullable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * 为玩家匹配对手
 *
 * @author NightDW 2023/11/7 15:41
 */
public interface Matcher<P extends Player> {

    /**
     * 找到与当前玩家匹配的对手；必须满足双向匹配；如果找不到，则会阻塞等待
     *
     * @param player   当前玩家
     * @param strategy 当前玩家的匹配策略
     * @return         与当前玩家彼此匹配的对手
     */
    @Nullable
    Result<P> match(P player, MatchStrategy strategy);

    /**
     * 判断指定玩家是否正在匹配中
     *
     * @param player 目标玩家
     * @return       该玩家是否正在匹配
     */
    boolean isMatching(P player);

    /**
     * 获取指定玩家的匹配策略
     *
     * @param player 目标玩家
     * @return       如果该玩家正在匹配，则返回其匹配策略，否则返回null
     */
    @Nullable
    MatchStrategy getMatchStrategy(P player);

    /**
     * 玩家可以通过该方法取消等待匹配
     *
     * @param player 要取消等待的玩家
     */
    void cancel(P player);

    /**
     * 用于支持异步匹配对手
     */
    default Executor getWaitingExecutor() {
        throw new UnsupportedOperationException();
    }

    /**
     * 异步匹配对手
     */
    default CompletableFuture<Result<P>> matchAsync(P player, MatchStrategy strategy) {
        return CompletableFuture.supplyAsync(() -> match(player, strategy), getWaitingExecutor());
    }

    /**
     * 匹配结果
     */
    @Getter
    @AllArgsConstructor
    class Result<P extends Player> {

        /**
         * 匹配时使用的策略
         */
        private MatchStrategy strategy;

        /**
         * 匹配到的对手
         */
        private P opponent;

        /**
         * 当前玩家是否比对手先进行匹配，可作为是否执红的依据
         */
        private boolean first;
    }
}
