package com.laidw.chess.battle.match;

import com.laidw.chess.battle.Player;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.locks.LockSupport;

/**
 * 简单的匹配器
 *
 * @author NightDW 2023/11/7 15:43
 */
public class SimpleMatcher<P extends Player> implements Matcher<P> {

    /**
     * 正在匹配的玩家的信息；包括玩家的匹配策略和该玩家对应的线程
     */
    private static class MatchingInfo {
        public final Thread thread;
        public final MatchStrategy strategy;
        public MatchingInfo(MatchStrategy strategy) {
            this.thread = Thread.currentThread();
            this.strategy = strategy;
        }
    }

    /**
     * 用于存放未匹配到对手的玩家及其信息；根据插入顺序进行排序；操作时需要加锁
     */
    private final Map<P, MatchingInfo> waitingMap = new LinkedHashMap<>();

    /**
     * 用于存放匹配结果，主要用于线程间的通信；不需要加锁
     */
    private final Map<P, Result<P>> resultMap = new ConcurrentHashMap<>();

    /**
     * 最多允许多少个玩家在等待匹配（避免太多线程阻塞）
     */
    private final int maxWaitingPlayers;

    /**
     * 执行异步匹配的线程池
     */
    private final ThreadPoolExecutor waitingThreadPool;

    public SimpleMatcher() {
        this(30);
    }

    public SimpleMatcher(int maxWaitingPlayers) {
        this.maxWaitingPlayers = maxWaitingPlayers;

        // 核心线程数、最大线程数都设置成最大等待数量 * 1.1
        // 确保线程池中至少有一部分线程能无阻塞地处理任务队列中的任务
        int threadCount = (int) (1.1 * maxWaitingPlayers);
        this.waitingThreadPool = new ThreadPoolExecutor(
                threadCount, threadCount,
                0L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(),
                new ThreadPoolExecutor.AbortPolicy()
        );
    }

    @Override
    public Result<P> match(P player, MatchStrategy strategy) {
        Map.Entry<P, MatchingInfo> matchedEntry = null;

        // 加锁，然后遍历整个等待集合
        synchronized (this) {

            // 避免同一个用户发起多个匹配请求，并且确保该用户的上一个匹配结果已被处理
            if (isMatching(player)) {
                throw new IllegalStateException("该玩家正在匹配中，无法再匹配");
            }

            // 获取与当前玩家相互匹配的对手
            for (Map.Entry<P, MatchingInfo> entry : waitingMap.entrySet()) {
                if (strategy.matches(player, entry.getKey()) && entry.getValue().strategy.matches(entry.getKey(), player)) {
                    matchedEntry = entry;
                    break;
                }
            }

            // 如果找不到，则将该玩家添加到待匹配集合中；否则，将对手从待匹配集合中移除
            if (matchedEntry == null) {
                if (waitingMap.size() >= maxWaitingPlayers) {
                    throw new RejectedExecutionException("等待的玩家过多");
                }
                waitingMap.put(player, new MatchingInfo(strategy));
            } else {
                waitingMap.remove(matchedEntry.getKey());
            }
        }

        // 如果找到了对手，则为对手线程生成匹配结果，并将结果存放到resultMap中，然后将其唤醒
        if (matchedEntry != null) {
            P opponent = matchedEntry.getKey();
            MatchingInfo opponentInfo = matchedEntry.getValue();
            resultMap.put(opponent, new Result<>(opponentInfo.strategy, player, true));
            LockSupport.unpark(opponentInfo.thread);
            return new Result<>(strategy, opponent, false);
        }

        // 否则，进入休眠，等待被匹配到的对手唤醒；被唤醒之后，读取并删除resultMap集合中的结果即可
        LockSupport.park();
        return resultMap.remove(player);
    }

    @Override
    public synchronized boolean isMatching(P player) {
        return waitingMap.containsKey(player) || resultMap.containsKey(player);
    }

    @Override
    public synchronized MatchStrategy getMatchStrategy(P player) {
        MatchingInfo matchingInfo = waitingMap.get(player);
        if (matchingInfo != null) {
            return matchingInfo.strategy;
        }
        Result<P> result = resultMap.get(player);
        return result == null ? null : result.getStrategy();
    }

    @Override
    public void cancel(P player) {
        MatchingInfo matchingInfo;
        synchronized (this) {
            matchingInfo = waitingMap.remove(player);
        }
        if (matchingInfo != null) {
            LockSupport.unpark(matchingInfo.thread);
        }
    }

    @Override
    public Executor getWaitingExecutor() {
        return this.waitingThreadPool;
    }
}
