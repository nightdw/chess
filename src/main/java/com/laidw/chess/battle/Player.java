package com.laidw.chess.battle;

/**
 * 玩家
 *
 * @author NightDW 2023/11/7 15:40
 */
public interface Player {

    /**
     * 获取该玩家的积分；积分可以作为匹配对手的依据
     *
     * @return 该玩家的积分
     */
    int getScore();
}
