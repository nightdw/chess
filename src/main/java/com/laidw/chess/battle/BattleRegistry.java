package com.laidw.chess.battle;

import com.laidw.chess.battle.match.Matcher;
import org.springframework.lang.Nullable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 对战大厅
 *
 * @author NightDW 2023/11/30 19:49
 */
public class BattleRegistry<P extends Player> {

    /**
     * 玩家及其进行中的对战
     */
    private final Map<P, Battle> battleMap = new ConcurrentHashMap<>();

    /**
     * 用于确定谁持红方
     */
    private final ColorDeducer<P> colorDeducer;

    public BattleRegistry(ColorDeducer<P> colorDeducer) {
        this.colorDeducer = colorDeducer;
    }

    /**
     * 获取指定玩家的对战信息；如果没有，则为其和对手创建对战信息
     *
     * @param player      目标玩家
     * @param matchResult 该玩家的匹配结果
     */
    public synchronized Battle getOrCreateBattle(P player, Matcher.Result<P> matchResult) {
        P opponent = matchResult.getOpponent();
        Battle battle = battleMap.get(player);
        Battle opponentBattle = battleMap.get(opponent);

        // battle和opponentBattle要么都为空，要么都不为空且相同
        if (battle != opponentBattle) {
            throw new IllegalStateException("当前玩家或对手有未结束的对战");
        }

        // 如果没有对战，则先创建
        if (battle == null) {
            if (colorDeducer.isRed(player, matchResult)) {
                battle = new Battle(player, opponent);
            } else {
                battle = new Battle(opponent, player);
            }
            battleMap.put(player, battle);
            battleMap.put(opponent, battle);
        }

        return battle;
    }

    /**
     * 删除指定玩家及其对手的对战
     *
     * @param player 目标玩家
     */
    @SuppressWarnings("SuspiciousMethodCalls")
    public synchronized void remove(P player) {
        Battle battle = battleMap.remove(player);
        if (battle != null && battleMap.remove(battle.getOpponentOf(player)) != battle) {
            throw new IllegalStateException("程序有误，对战信息未成对出现");
        }
    }

    @Nullable
    public Battle getBattle(P player) {
        return battleMap.get(player);
    }

    public Battle getRequiredBattle(P player) {
        Battle battle = getBattle(player);
        if (battle == null) {
            throw new RuntimeException("不存在的对战信息");
        }
        return battle;
    }
}
