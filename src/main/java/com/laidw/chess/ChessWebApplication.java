package com.laidw.chess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * Description of class {@link ChessWebApplication}.
 *
 * @author NightDW 2023/10/7 1:41
 */
@SpringBootApplication
@Import(ServerEndpointExporter.class)
public class ChessWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(ChessWebApplication.class, args);
    }
}
