package com.laidw.chess.util;

import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;

/**
 * Basic认证工具类
 *
 * @author NightDW 2023/12/1 18:36
 */
public class BasicAuthUtil {

    /**
     * 响应错误信息，让用户登录
     */
    public static void sendNoAuthError(HttpServletResponse response) {
        response.setStatus(401);
        response.addHeader("WWW-Authenticate", "basic realm='no auth'");
    }

    /**
     * 解析用户信息，包括用户名、密码；用数组表示
     */
    @Nullable
    public static String[] parseAuth(HttpServletRequest request) {
        String auth = request.getHeader("Authorization");
        if (auth == null || auth.isEmpty()) {
            return null;
        }
        return new String(Base64.getDecoder().decode(auth.substring(6).getBytes())).split(":");
    }
}
