package com.laidw.chess.util;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.common.Color;
import com.laidw.chess.common.Position;
import com.laidw.chess.common.Role;
import com.laidw.chess.piece.Piece;
import com.laidw.chess.solution.*;

import java.util.*;

/**
 * Description of class {@link SolutionUtil}.
 *
 * @author NightDW 2023/10/11 0:52
 */
public class SolutionUtil {
    private static final Map<Role, Solution> solutionMap = new HashMap<>();
    static {
        solutionMap.put(Role.SOLDIER, SoldierSolution.INSTANCE);
        solutionMap.put(Role.CANNON, CannonSolution.INSTANCE);
        solutionMap.put(Role.CHARIOT, ChariotSolution.INSTANCE);
        solutionMap.put(Role.HORSE, HorseSolution.INSTANCE);
    }

    private static Solution getSolution(Piece attacker) {
        return solutionMap.get(attacker.getRole());
    }

    private static class WeightedAction extends Action {
        final int weight;
        public WeightedAction(Position from, Position to, int weight) {
            super(from, to);
            this.weight = weight;
        }
    }

    /**
     * 已知color方被将军（可能有多个将军），获取解将的方法
     */
    public static Collection<Action> getSolutions(Chess chess, Color color) {
        Collection<Action> interactions = null;
        Piece king = chess.getKing(color);
        for (Piece attacker : king.getAttackers()) {
            Collection<Action> actions = getSolution(attacker).solveCheck(chess, attacker, king);
            if (interactions == null) {
                interactions = actions;
            } else {
                interactions.retainAll(actions);
            }
            if (interactions.isEmpty()) {
                return Collections.emptyList();
            }
        }

        // 对得到的解法进行进一步过滤：
        // 1. 如果采用相应的行动后我方的将帅还是被攻击，则忽略该解法
        // 2. 否则，计算移动之后对手能够到将的方案数，方案数越小，优先级越高
        assert interactions != null;
        Color opponent = color.reverse();
        List<Action> weightedSolutions = new ArrayList<>(interactions.size());
        for (Action solution : interactions) {
            chess.move(solution.from, solution.to);

            // 此时我方将帅的攻击者必须是空，然后计算对方存活的车马炮兵总共能够到将的方案数
            if (king.getAttackers().isEmpty()) {
                int count = 0;
                Iterator<Piece> opponentPieceItr = chess.getPieces(opponent, p -> p.getRole().aggressive);
                while (opponentPieceItr.hasNext()) {
                    count += opponentPieceItr.next().getAttackOrProtectActions(king).size();
                }
                weightedSolutions.add(new WeightedAction(solution.from, solution.to, count));
            }

            chess.back();
        }

        weightedSolutions.sort(Comparator.comparingInt(a -> ((WeightedAction)a).weight));
        return weightedSolutions;
    }
}
