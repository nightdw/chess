package com.laidw.chess.util;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Color;
import com.laidw.chess.common.Position;
import com.laidw.chess.common.Role;

import java.util.*;

/**
 * 棋盘的默认布局
 *
 * @author NightDW 2023/10/7 0:38
 */
public class DefaultLayout {
    private static final Map<Role, List<Position.Relative>> piecePositionsMap = new HashMap<>();
    static {
        piecePositionsMap.put(Role.SOLDIER, Arrays.asList(
                new Position.Relative(3, 0),
                new Position.Relative(3, 2),
                new Position.Relative(3, 4),
                new Position.Relative(3, 6),
                new Position.Relative(3, 8)
        ));

        piecePositionsMap.put(Role.CANNON, Arrays.asList(
                new Position.Relative(2, 1),
                new Position.Relative(2, 7)
        ));

        piecePositionsMap.put(Role.CHARIOT, Arrays.asList(
                new Position.Relative(0, 0),
                new Position.Relative(0, 8)
        ));

        piecePositionsMap.put(Role.HORSE, Arrays.asList(
                new Position.Relative(0, 1),
                new Position.Relative(0, 7)
        ));

        piecePositionsMap.put(Role.ELEPHANT, Arrays.asList(
                new Position.Relative(0, 2),
                new Position.Relative(0, 6)
        ));

        piecePositionsMap.put(Role.GUARD, Arrays.asList(
                new Position.Relative(0, 3),
                new Position.Relative(0, 5)
        ));

        piecePositionsMap.put(Role.KING, Collections.singletonList(
                new Position.Relative(0, 4)
        ));
    }

    public static void init(Chess chess) {
        piecePositionsMap.forEach((role, positions) -> {
            for (Position.Relative position : positions) {
                chess.put(position.toAbsolute(Color.BLACK), Color.BLACK, role);
                chess.put(position.toAbsolute(Color.RED), Color.RED, role);
            }
        });
    }
}
