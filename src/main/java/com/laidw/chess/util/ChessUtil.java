package com.laidw.chess.util;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Direction;
import com.laidw.chess.common.Position;
import com.laidw.chess.piece.Piece;
import org.springframework.lang.Nullable;

import java.util.function.Consumer;

/**
 * Description of class {@link ChessUtil}.
 *
 * @author NightDW 2023/10/10 21:05
 */
public class ChessUtil {

    /**
     * 从目标位置开始（不包含），沿着某个方向扫描空格，并处理这些空格
     *
     * @return 当返回null时，说明扫描到尽头了还没遇到其它棋子，否则说明因为遇到了该棋子而中断了
     */
    @Nullable
    public static Piece scanBlank(Chess chess, Position from, Direction direction, @Nullable Consumer<Position> blankPositionConsumer) {
        Position cur = direction.next(from);
        Piece block = null;
        while (cur != null && (block = chess.get(cur)) == null) {
            if (blankPositionConsumer != null) {
                blankPositionConsumer.accept(cur);
            }
            cur = direction.next(cur);
        }
        return block;
    }
}
