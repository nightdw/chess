package com.laidw.chess.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Description of class {@link CollectionUtil}.
 *
 * @author NightDW 2023/10/11 17:19
 */
public class CollectionUtil {
    public static <T> List<T> toList(Iterator<T>  iterator) {
        List<T> list = new ArrayList<>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }
}
