package com.laidw.chess.util;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.common.Color;
import org.springframework.lang.Nullable;

import java.util.*;

/**
 * 通过连续将军来取胜
 *
 * @author NightDW 2023/10/10 18:07
 */
public class VccUtil {
    private static final int MAX_DEPTH = 30;
    private static final Action LOSE = new Action(null, null);
    private static final Action UNKNOWN = new Action(null, null);
    private static final Action SUCCESS = new Action(null, null);
    private static final Action SUCCESS_SIMPLE = new Action(null, null);

    /**
     * 尝试通过连续将军来取胜
     */
    @Nullable
    public static Action tryVcc(Chess chess, Color me) {

        // snapshots集合用于记录当前路径下的所有节点的快照，避免在搜索中重复判断
        // 但是，单次vcc不会搜索到重复结果，不代表下一次vcc时不会重新进入这个重复的局面
        //
        // 假设我方当前有两种叫将方式：
        // 1. 车将，此时可以绝杀
        // 2. 移动炮1使其成为炮2的炮架，用炮2将；此时对方只能将炮3垫在两个炮之间，并且如果移走炮1，则对手只能移走炮3
        //
        // 这里在vcc时，优先使用炮叫将
        // 那么，在单次vcc中，由于有重复判断，因此可以自动推断出：炮1当炮架将 -> 垫炮3 -> 移走炮1 -> 移走炮3 -> 车将
        // 当执行了 炮1当炮架将 -> 垫炮3 后，再进行vcc时，可以自动推断出：移走炮1 -> 移走炮3 -> 车将
        // 当执行了 移走炮1 -> 移走炮3 后，再进行vcc时，由于优先使用炮将，因此此时依旧会推断出：炮1当炮架将 -> 垫炮3 -> 移走炮1 -> 移走炮3 -> 车将
        // 这样一来，多个vcc操作之间就出现了重复
        //
        // 因此，这里需要将棋盘的历史快照也存进来；只需要放对手落子后的快照，并且最后一个快照不能放进来（它是当前快照）
        Set<Chess.Snapshot> snapshots = new HashSet<>();
        List<Chess.Snapshot> historySnapshots = chess.getHistorySnapshots();
        for (int i = historySnapshots.size() - 3; i >= 0; i -= 2) {
            snapshots.add(historySnapshots.get(i));
        }

        Action action = tryVccInternal(chess, me, snapshots, new HashSet<>(), chess.getActionCount() + MAX_DEPTH);
        if (action == SUCCESS || action == SUCCESS_SIMPLE) {
            throw new IllegalStateException("出现了不该出现的中间结果");
        }
        return action == LOSE || action == UNKNOWN ? null : action;
    }

    /**
     * 尝试通过连续将军来取胜
     *
     * @param snapshots 当前搜索路径上的所有祖先节点的快照；用于去重
     * @param successes 用于存放整个搜索过程中遇到的所有必赢的局面；用于加快搜索速度
     * @param maxActCnt 棋盘的最大操作数量，用于限制搜索的最大深度
     * @return          {@link #LOSE}代表失败，{@link #UNKNOWN}代表不确定，其它都代表成功
     */
    private static Action tryVccInternal(Chess chess, Color me, Set<Chess.Snapshot> snapshots, Set<Chess.Snapshot> successes, final int maxActCnt) {

        // 将当前快照记录下来（方法结束时需删除该快照）
        // 如果当前局面已经判断过了，则局势不明朗
        // 这是因为当前局面的上一步是对手走的最优解，因此，如果出现了重复局势，那么双方只会一直僵持，因此我方必须主动求变
        Chess.Snapshot snapshot = chess.getSnapshot();
        if (!snapshots.add(snapshot)) {
            return UNKNOWN;
        }

        // 如果之前已经计算出该局面必定能赢，则直接返回成功
        if (successes.contains(snapshot)) {
            snapshots.remove(snapshot);
            return SUCCESS;
        }

        // 如果我方有将军，则获胜
        // 注意，这种情况是必赢的，但没有必要记录下来，避免占用太多空间
        // 同时，返回的是SUCCESS_SIMPLE，以告诉调用者，不需要保存此次成功的快照
        Color opponent = me.reverse();
        if (chess.isChecked(opponent)) {
            snapshots.remove(snapshot);
            return SUCCESS_SIMPLE;
        }

        // 否则，如果对方有将军，则尝试解将还将；如果成功，则记录下当前局面
        if (chess.isChecked(me)) {
            Action action = trySolveAndPayBack(chess, me, snapshots, successes, maxActCnt);
            snapshots.remove(snapshot);
            if (action != UNKNOWN && action != LOSE) {
                successes.add(snapshot);
            }
            return action;
        }

        // 此时需要往下搜索，因此，如果已经达到最大深度了，则不能再搜索下去了
        if (chess.getActionCount() >= maxActCnt) {
            snapshots.remove(snapshot);
            return UNKNOWN;
        }

        // 尝试叫将，并枚举对手所有的应法，如果不管对手怎么应，我方都能vcc成功，则此次叫将是可行的
        Iterator<Action> checkActions = chess.getCheckActions(me);
        check : while (checkActions.hasNext()) {
            Action action = checkActions.next();
            chess.move(action.from, action.to);

            // 不能送将
            if (chess.isChecked(me)) {
                chess.back();
                continue;
            }

            // 枚举对手的所有操作，然后继续vcc
            Collection<Action> solutions = SolutionUtil.getSolutions(chess, opponent);
            for (Action solution : solutions) {
                chess.move(solution.from, solution.to);
                Action result = tryVccInternal(chess, me, snapshots, successes, maxActCnt);

                // 如果没法vcc成功，则直接尝试下一个叫将
                if (result == UNKNOWN || result == LOSE) {
                    chess.back();
                    chess.back();
                    continue check;
                }

                // 否则，说明执行这两步后我方是必赢的局面，因此如果该局面不是简单的局面，则记录下来
                if (action != SUCCESS_SIMPLE) {
                    successes.add(chess.getSnapshot());
                }

                // 准备尝试对手的下一个解法
                chess.back();
            }

            // 执行到这里，说明不管对手怎么回应，我方都能取胜，因此记录下当前局面，并返回此次解法
            successes.add(snapshot);
            chess.back();
            snapshots.remove(snapshot);
            return action;
        }

        // 如果所有的叫将操作都不可行，则局势不明朗
        snapshots.remove(snapshot);
        return UNKNOWN;
    }

    /**
     * 尝试解将还将
     */
    private static Action trySolveAndPayBack(Chess chess, Color me, Set<Chess.Snapshot> snapshots, Set<Chess.Snapshot> successes, final int maxActCnt) {
        Collection<Action> solutions = SolutionUtil.getSolutions(chess, me);
        Color opponent = me.reverse();

        // 枚举所有的解法
        solve : for (Action solution : solutions) {
            chess.move(solution.from, solution.to);

            // 如果此次解法没法反将，则跳过
            if (chess.isChecked(me) || !chess.isChecked(opponent)) {
                chess.back();
                continue;
            }

            // 否则，再枚举对手的所有解法
            Collection<Action> opponentSolutions = SolutionUtil.getSolutions(chess, opponent);
            for (Action opponentSolution : opponentSolutions) {
                chess.move(opponentSolution.from, opponentSolution.to);
                Action result = tryVccInternal(chess, me, snapshots, successes, maxActCnt);

                // 如果没法vcc成功，则直接尝试下一个叫将
                if (result == UNKNOWN || result == LOSE) {
                    chess.back();
                    chess.back();
                    continue solve;
                }

                // 否则，说明执行这两步后我方是必赢的局面，因此如果该局面不是简单的局面，则记录下来
                if (result != SUCCESS_SIMPLE) {
                    successes.add(chess.getSnapshot());
                }

                // 准备尝试对手的下一个解法
                chess.back();
            }

            // 执行到这里，说明不管对手怎么回应，我方都能取胜，因此返回此次解法
            // 这里不需要保存这个必赢的局面，因为调用者会自动保存
            chess.back();
            return solution;
        }

        return solutions.isEmpty() ? LOSE : UNKNOWN;
    }
}
