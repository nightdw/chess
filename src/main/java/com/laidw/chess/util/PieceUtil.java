package com.laidw.chess.util;

import com.laidw.chess.common.Action;
import com.laidw.chess.common.Position;
import com.laidw.chess.piece.Piece;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;

/**
 * Description of class {@link PieceUtil}.
 *
 * @author NightDW 2023/10/10 21:13
 */
public class PieceUtil {

    /**
     * 已知p1、p2和piece在同一行或同一列上；本方法负责找到能把piece移出该行或该列的动作
     */
    public static void addLeaveLineActions(Position p1, Position p2, Piece piece, Collection<Action> list) {

        // 判断是在同一行还是同一列
        boolean sameRow = p1.x == p2.x;
        for (Position nextPosition : piece.getNextPositions()) {
            if ((sameRow && p1.x != nextPosition.x) || (!sameRow && p1.y != nextPosition.y)) {
                list.add(new Action(piece.getPosition(), nextPosition));
            }
        }
    }

    /**
     * 已知p1和p2位于同一行/列，本方法会返回一个过滤器，该过滤器会过滤出在p1和p2之间的所有坐标
     */
    public static Predicate<Position> getBetweenFilter(Position p1, Position p2) {
        int x1 = Math.min(p1.x, p2.x), x2 = Math.max(p1.x, p2.x);
        int y1 = Math.min(p1.y, p2.y), y2 = Math.max(p1.y, p2.y);
        return x1 == x2 ?
                p -> p.x == x1 && p.y > y1 && p.y <= y2 :
                p -> p.y == y1 && p.x > x1 && p.x <= x2;
    }

    public static void addCanMoveToTargetActions(Iterator<Piece> pieceIterator, Position target, Collection<Action> list) {
        while (pieceIterator.hasNext()) {
            Piece piece = pieceIterator.next();
            if (piece.canMoveTo(target)) {
                list.add(new Action(piece.getPosition(), target));
            }
        }
    }
}
