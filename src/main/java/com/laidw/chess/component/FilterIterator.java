package com.laidw.chess.component;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Predicate;

/**
 * 带有过滤功能的迭代器
 *
 * @author NightDW 2023/10/10 23:55
 */
@SuppressWarnings("unchecked")
public class FilterIterator<T> implements Iterator<T> {
    private static final Object NOT_FETCHED = new Object();

    private final Iterator<T> delegate;
    private final Predicate<T> filter;
    private Object cur = NOT_FETCHED;

    public FilterIterator(Iterator<T> delegate, Predicate<T> filter) {
        this.delegate = delegate;
        this.filter = filter;
    }

    @Override
    public boolean hasNext() {
        while (cur == NOT_FETCHED || !filter.test((T) cur)) {
            if (!delegate.hasNext()) {
                return false;
            }
            cur = delegate.next();
        }
        return true;
    }

    @Override
    public T next() {
        T cur = (T) this.cur;
        this.cur = NOT_FETCHED;
        return cur;
    }

    /**
     * 测试程序
     */
    public static void main(String[] args) {
        Iterator<Integer> iterator = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, null).iterator();
        iterator = new FilterIterator<>(iterator, i -> i != null && (i & 1) == 1);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
