package com.laidw.chess.component;

import com.laidw.chess.common.Color;
import com.laidw.chess.common.Role;
import com.laidw.chess.piece.*;
import lombok.Getter;

import java.util.*;

/**
 * 棋子计数器；用于统计某个棋子被哪些棋子保护或攻击，等等
 *
 * @author NightDW 2023/10/6 15:28
 */
public class PieceCounter implements Iterable<Piece> {
    private final List<List<Piece>> piecesGroupByRole;
    {
        final int length = Role.values().length;
        piecesGroupByRole = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            piecesGroupByRole.add(new ArrayList<>(2));
        }
    }

    @Getter
    private int count = 0;

    private int modifyCount = 0;

    public void add(Piece piece) {
        if (piecesGroupByRole.get(piece.getRole().ordinal()).contains(piece)) {
            throw new IllegalStateException();
        }
        modifyCount++;
        count++;
        piecesGroupByRole.get(piece.getRole().ordinal()).add(piece);
    }

    public void remove(Piece piece) {
        if (!piecesGroupByRole.get(piece.getRole().ordinal()).remove(piece)) {
            throw new IllegalStateException();
        }
        modifyCount++;
        count--;
    }

    public boolean contains(Role role) {
        return !piecesGroupByRole.get(role.ordinal()).isEmpty();
    }

    public List<Piece> get(Role role) {
        return Collections.unmodifiableList(piecesGroupByRole.get(role.ordinal()));
    }

    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public Iterator<Piece> iterator() {
        return new Iterator<Piece>() {
            private final int expectedModifyCount = modifyCount;
            private int fetchCount = 0, groupIdx = 0, curIdx = 0;

            @Override
            public boolean hasNext() {
                ensureNotModify();
                return fetchCount < count;
            }

            @Override
            public Piece next() {
                ensureNotModify();
                fetchCount++;
                while (curIdx >= piecesGroupByRole.get(groupIdx).size()) {
                    groupIdx++;
                    curIdx = 0;
                }
                return piecesGroupByRole.get(groupIdx).get(curIdx++);
            }

            private void ensureNotModify() {
                if (expectedModifyCount != modifyCount) {
                    throw new ConcurrentModificationException();
                }
            }
        };
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "[]";
        }

        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (List<Piece> pieces : piecesGroupByRole) {
            for (Piece piece : pieces) {
                sb.append(piece.getName()).append(' ');
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(']');
        return sb.toString();
    }

    /**
     * 测试迭代器和toString()方法
     */
    public static void main(String[] args) {
        PieceCounter pieces = new PieceCounter();
        pieces.add(new SoldierPiece(Color.BLACK, null, null));
        pieces.add(new HorsePiece(Color.BLACK, null, null));
        pieces.add(new SoldierPiece(Color.BLACK, null, null));
        pieces.add(new ChariotPiece(Color.BLACK, null, null));
        pieces.add(new KingPiece(Color.BLACK, null, null));
        for (Piece piece : pieces) {
            System.out.println(piece.getName());
        }
        System.out.println(pieces);
    }
}
