package com.laidw.chess.component;

import lombok.AllArgsConstructor;

import java.util.*;
import java.util.function.Function;

/**
 * 二维数组迭代器，支持嵌套数组的懒初始化
 *
 * @author NightDW 2023/10/10 18:15
 */
public class LazyMatrixIterator<S, T> implements Iterator<T> {
    private final List<S> sources;
    private final Function<S, List<T>> targetsMapper;

    private int sourceIdx = 0, targetIdx = 0;
    private List<T> targets = null;

    public LazyMatrixIterator(List<S> sources, Function<S, List<T>> targetsMapper) {
        this.sources = sources;
        this.targetsMapper = targetsMapper;
    }

    @Override
    public boolean hasNext() {
        while (targets == null || targetIdx >= targets.size()) {
            if (sourceIdx >= sources.size()) {
                return false;
            }
            targets = targetsMapper.apply(sources.get(sourceIdx++));
            targetIdx = 0;
        }
        return true;
    }

    @Override
    public T next() {
        return targets.get(targetIdx++);
    }

    /**
     * 测试程序
     */
    public static void main(String[] args) {

        @AllArgsConstructor
        class IntegersGetter {
            private final List<Integer> list;
        }

        List<IntegersGetter> list = new ArrayList<>();
        list.add(new IntegersGetter(Arrays.asList(1, 2, 3)));
        list.add(new IntegersGetter(null));
        list.add(new IntegersGetter(Arrays.asList(4, 5, 6)));
        list.add(new IntegersGetter(Collections.emptyList()));
        list.add(new IntegersGetter(Arrays.asList(7, 8, 9)));

        Iterator<Integer> iterator = new LazyMatrixIterator<>(list, get -> get.list);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
