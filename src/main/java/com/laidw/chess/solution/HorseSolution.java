package com.laidw.chess.solution;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.common.Position;
import com.laidw.chess.piece.Piece;
import com.laidw.chess.util.PieceUtil;

import java.util.Set;

/**
 * 处理马将
 *
 * @author NightDW 2023/10/10 23:46
 */
public class HorseSolution extends Solution {
    public static final HorseSolution INSTANCE = new HorseSolution();

    private HorseSolution() {}

    /**
     * 只要任意移动一次将即可
     */
    @Override
    protected void addAvoidingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
        solutions.addAll(king.getAllActions());
    }

    /**
     * 只要将我方棋子（除了将）移动到能别马腿的地方即可
     */
    @Override
    protected void addBlockingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
        PieceUtil.addCanMoveToTargetActions(chess.getPieces(king.getColor(), p -> p != king), Position.horseFulcrum(attacker.getPosition(), king.getPosition()), solutions);
    }
}
