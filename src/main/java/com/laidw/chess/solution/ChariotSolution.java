package com.laidw.chess.solution;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.common.Position;
import com.laidw.chess.piece.Piece;
import com.laidw.chess.util.PieceUtil;

import java.util.Set;
import java.util.function.Predicate;

/**
 * 解决车将
 *
 * @author NightDW 2023/10/11 0:17
 */
public class ChariotSolution extends Solution {
    public static final ChariotSolution INSTANCE = new ChariotSolution();

    private ChariotSolution() {}

    /**
     * 只要把将移到别的行/列上即可
     */
    @Override
    protected void addAvoidingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
        PieceUtil.addLeaveLineActions(attacker.getPosition(), king.getPosition(), king, solutions);
    }

    /**
     * 只要将我方棋子（除了将）移动到将和车之间即可
     */
    @Override
    protected void addBlockingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
        Predicate<Position> filter = PieceUtil.getBetweenFilter(attacker.getPosition(), king.getPosition());
        for (Piece friend : chess.getAllPieces(king.getColor())) {
            if (friend != king) {
                solutions.addAll(friend.getActions(filter));
            }
        }
    }
}
