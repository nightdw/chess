package com.laidw.chess.solution;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.common.Direction;
import com.laidw.chess.common.Position;
import com.laidw.chess.piece.Piece;
import com.laidw.chess.util.ChessUtil;
import com.laidw.chess.util.PieceUtil;

import java.util.Set;
import java.util.function.Predicate;

/**
 * 解决炮将
 *
 * @author NightDW 2023/10/11 0:17
 */
public class CannonSolution extends Solution {
    public static final CannonSolution INSTANCE = new CannonSolution();

    private CannonSolution() {}

    /**
     * 只要把将移到别的行/列上即可
     */
    @Override
    protected void addAvoidingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
        PieceUtil.addLeaveLineActions(attacker.getPosition(), king.getPosition(), king, solutions);
    }

    /**
     * 只要将我方棋子（除了将）移动到将和炮之间（除了炮架）即可；另外，还需要额外考虑两种情况
     */
    @Override
    protected void addBlockingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
        Direction direction = Direction.getNormal(attacker.getPosition(), king.getPosition());
        Piece fulcrum = ChessUtil.scanBlank(chess, attacker.getPosition(), direction, null);
        assert fulcrum != null;

        Predicate<Position> filter = PieceUtil.getBetweenFilter(attacker.getPosition(), king.getPosition())
                .and(p -> p != fulcrum.getPosition());
        for (Piece friend : chess.getAllPieces(king.getColor())) {
            if (friend != king) {
                solutions.addAll(friend.getActions(filter));
            }
        }

        // 如果炮架是对方的，那么可以让将吃掉炮架
        if (king.isOpponent(fulcrum)) {
            if (king.canMoveTo(fulcrum.getPosition())) {
                solutions.add(new Action(king.getPosition(), fulcrum.getPosition()));
            }

        // 否则，拆开炮架
        } else {
            PieceUtil.addLeaveLineActions(attacker.getPosition(), king.getPosition(), fulcrum, solutions);
        }
    }
}
