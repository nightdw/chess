package com.laidw.chess.solution;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.piece.Piece;
import com.laidw.chess.util.PieceUtil;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 处理对手的将军
 *
 * @author NightDW 2023/10/10 23:35
 */
public abstract class Solution {

    /**
     * 已知attacker攻击着king，获取到能够处理此次将军的动作
     */
    public Collection<Action> solveCheck(Chess chess, Piece attacker, Piece king) {
        Set<Action> solutions = new LinkedHashSet<>();

        // 可以用队友（包括自己）来吃掉攻击者
        addDestroyAttackerActions(chess, attacker, king, solutions);

        // 也可以移动将帅，避开攻击者的攻击范围
        addAvoidingActions(chess, attacker, king, solutions);

        // 还可以通过垫子或别马腿的方式来解决
        addBlockingActions(chess, attacker, king, solutions);

        return solutions;
    }

    private void addDestroyAttackerActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
        PieceUtil.addCanMoveToTargetActions(chess.getAllPieces(king.getColor()).iterator(), attacker.getPosition(), solutions);
    }

    protected abstract void addAvoidingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions);

    protected abstract void addBlockingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions);
}
