package com.laidw.chess.solution;

import com.laidw.chess.Chess;
import com.laidw.chess.common.Action;
import com.laidw.chess.piece.Piece;

import java.util.Set;

/**
 * 解决兵将
 *
 * @author NightDW 2023/10/11 0:17
 */
public class SoldierSolution extends Solution {
    public static final SoldierSolution INSTANCE = new SoldierSolution();

    private SoldierSolution() {}

    /**
     * 只要任意移动一次将即可；这里动将时可能会顺便把兵吃掉了，因此可能会有重复结果
     */
    @Override
    protected void addAvoidingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
        solutions.addAll(king.getAllActions());
    }

    /**
     * 无法通过垫子的方式解决兵将
     */
    @Override
    protected void addBlockingActions(Chess chess, Piece attacker, Piece king, Set<Action> solutions) {
    }
}
