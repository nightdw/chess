package com.laidw.chess.service;

import com.laidw.chess.entity.User;
import org.springframework.lang.Nullable;

/**
 * 用于加载用户信息；登录时使用
 *
 * @author NightDW 2023/12/1 17:01
 */
public interface UserService {

    @Nullable
    User load(String username, String password);

    User get(Long userId);
}
