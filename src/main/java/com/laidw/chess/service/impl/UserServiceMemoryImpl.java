package com.laidw.chess.service.impl;

import com.laidw.chess.entity.User;
import com.laidw.chess.service.UserService;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * 为了方便，直接将用户信息保存在内存中
 *
 * @author NightDW 2023/12/1 17:02
 */
@Service
public class UserServiceMemoryImpl implements UserService {
    private static final List<User> users = Arrays.asList(
            newUser(1L, "zs", "333", 100),
            newUser(2L, "ls", "444", 70),
            newUser(3L, "ww", "555", 91),
            newUser(4L, "zl", "666", 86)
    );

    @Override
    @Nullable
    public User load(String username, String password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User get(Long userId) {
        for (User user : users) {
            if (user.getUserId().equals(userId)) {
                return user;
            }
        }
        throw new NoSuchElementException("用户不存在");
    }

    private static User newUser(Long userId, String username, String password, Integer score) {
        User user = new User();
        user.setUserId(userId);
        user.setUsername(username);
        user.setPassword(password);
        user.setScore(score);
        return user;
    }
}
