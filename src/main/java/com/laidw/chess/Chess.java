package com.laidw.chess;

import com.laidw.chess.common.Action;
import com.laidw.chess.common.Color;
import com.laidw.chess.common.Position;
import com.laidw.chess.common.Role;
import com.laidw.chess.component.FilterIterator;
import com.laidw.chess.component.LazyMatrixIterator;
import com.laidw.chess.component.PieceCounter;
import com.laidw.chess.piece.*;
import com.laidw.chess.util.CollectionUtil;
import com.laidw.chess.util.DefaultLayout;
import lombok.Getter;
import org.springframework.lang.Nullable;

import java.util.*;
import java.util.function.Predicate;

/**
 * 象棋棋盘
 *
 * @author NightDW 2023/10/5 0:53
 */
public class Chess {

    /**
     * 为了方便，允许外界直接获取棋盘信息
     */
    @Getter
    private final Piece[][] board = new Piece[Position.ROWS][Position.COLS];

    /**
     * 双方存活的棋子
     */
    private final PieceCounter aliveRedPieces = new PieceCounter();
    private final PieceCounter aliveBlackPieces = new PieceCounter();

    /**
     * 记录下所有的下棋动作，方便实现回退功能
     */
    private final List<Operation> operations = new ArrayList<>();

    /**
     * 记录下执行了不同操作后的棋盘的快照；最后一个就代表当前的快照；大小和{@link #operations}相同
     */
    @Getter
    private final List<Snapshot> historySnapshots = new ArrayList<>();

    /**
     * 当前棋盘的快照信息，会实时更新，主要用于提高计算快照的效率；不要暴露出去，也不要放到哈希表中
     */
    private final Snapshot modifiableSnapshot = new Snapshot(0L, 0L, 0L, 0L, 0L, 0L);

    /**
     * 用户的某次操作，以及此次操作吃掉的棋子
     */
    private static class Operation extends Action {

        @Nullable
        private final Piece oldPiece;

        public Operation(Position from, Position to, @Nullable Piece oldPiece) {
            super(from, to);
            this.oldPiece = oldPiece;
        }
    }

    /**
     * 当前棋盘的快照信息；一个棋子可以用4个比特位来表示，因此可以用6个long值来表示棋盘信息
     */
    public static final class Snapshot {
        private final long[] data = new long[6];

        public Snapshot(String data) {
            this(parseStr(data));
        }

        public Snapshot(long... data) {
            if (data.length != 6) {
                throw new IllegalArgumentException();
            }
            System.arraycopy(data, 0, this.data, 0, 6);
        }

        private Snapshot(Piece[][] board) {
            int idx = 0, counter = 0;
            long hash = 0L;
            for (int x = 0; x < Position.ROWS; x++) {
                for (int y = 0; y < Position.COLS; y++) {
                    hash = (hash << 4) | hash(board[x][y]);
                    if ((++counter & 15) == 0) {
                        data[idx++] = hash;
                        hash = 0L;
                    }
                }
            }
            data[idx] = hash << 24;
        }

        /**
         * 更新快照信息，仅限内部使用，并且被更新的快照不能存于哈希表中！
         */
        private void set(Position position, @Nullable Piece piece) {
            int i = position.x * Position.COLS + position.y;

            // 待更新的元素是data[longIdx]，并且更新的4个比特位后面有sufLen个比特位
            int longIdx = i >> 4, sufLen = 60 - ((i & 15) << 2);

            long hash = data[longIdx];
            data[longIdx] = ((((hash >> (sufLen + 4)) << 4) | hash(piece)) << sufLen) | (hash & ((1L << sufLen) - 1));
        }

        /**
         * 根据本快照来恢复棋盘
         */
        public Chess resume() {
            Chess chess = new Chess(false);
            int curIdx = 0, sufLen = 60;
            long hash = data[0];
            for (int x = 0; x < Position.ROWS; x++) {
                for (int y = 0; y < Position.COLS; y++) {
                    int pieceHash = (int) ((hash >> sufLen) & 15);

                    if (pieceHash != 0) {
                        chess.put(Position.of(x, y), Piece.parseColor(pieceHash), Piece.parseRole(pieceHash));
                    }

                    if ((sufLen -= 4) < 0) {
                        hash = data[++curIdx];
                        sufLen = 60;
                    }
                }
            }
            return chess;
        }

        private long hash(@Nullable Piece piece) {
            return piece == null ? 0L : Piece.hash(piece.getColor(), piece.getRole());
        }

        @Override
        public int hashCode() {
            int hash = 0;
            for (long l : data) {
                hash ^= (int) l;
                hash ^= (int) (l >> 32);
            }
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Snapshot) {
                long[] other = ((Snapshot) obj).data;
                for (int i = 0; i < 6; i++) {
                    if (data[i] != other[i]) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Long datum : data) {
                sb.append(datum).append(',');
            }
            return sb.substring(0, sb.length() - 1);
        }

        private static long[] parseStr(String data) {
            String[] split = data.split(",");
            long[] arr = new long[split.length];
            for (int i = 0; i < split.length; i++) {
                arr[i] = Long.parseLong(split[i]);
            }
            return arr;
        }
    }

    /**
     * 构造器；使用默认布局
     */
    public Chess() {
        this(true);
    }

    /**
     * 构造器；可以使用默认布局，也允许用户自定义棋子的{@link #put(Position, Color, Role) 摆放}和{@link #remove(Position) 移除}
     */
    public Chess(boolean useDefaultLayout) {
        if (useDefaultLayout) {
            DefaultLayout.init(this);
        }
    }

    /**
     * 将棋子摆放到棋盘上
     */
    public void put(Position position, Color color, Role role) {
        if (!operations.isEmpty()) {
            throw new IllegalStateException("对局已正式开始");
        }
        if (get(position) != null) {
            throw new IllegalStateException(position + "位置已存在棋子");
        }

        Piece piece = newPiece(color, role, position);
        bindPositionAndPiece(position, piece);
        getAlivePieceCounter(color).add(piece);
        refreshPieces(position);
    }

    /**
     * 将棋子从棋盘中移除
     */
    public void remove(Position position) {
        Piece piece;
        if (!operations.isEmpty()) {
            throw new IllegalStateException("对局已正式开始");
        }
        if ((piece = get(position)) == null) {
            throw new IllegalStateException(position + "位置不存在棋子");
        }

        bindPositionAndPiece(position, null);
        piece.clear();
        getAlivePieceCounter(piece.getColor()).remove(piece);
        refreshPieces(position);
    }

    /**
     * 移动棋子
     */
    public void move(Position from, Position to) {
        Piece piece = get(from);
        if (piece == null || !piece.canMoveTo(to)) {
            throw new IllegalStateException("起点" + from + "上没有棋子，或该棋子无法到达终点" + to);
        }

        Piece oldPiece = get(to);
        operations.add(new Operation(from, to, oldPiece));
        bindPositionAndPiece(from, null);
        bindPositionAndPiece(to, piece);
        historySnapshots.add(getSnapshot());
        if (oldPiece != null) {
            getAlivePieceCounter(oldPiece.getColor()).remove(oldPiece);
            oldPiece.clear();
        }
        refreshPieces(from, to);
    }

    /**
     * 撤回上一步操作
     */
    public void back() {
        if (operations.isEmpty()) {
            throw new IllegalStateException("无效操作");
        }

        Operation opt = operations.remove(operations.size() - 1);
        historySnapshots.remove(historySnapshots.size() - 1);
        bindPositionAndPiece(opt.from, get(opt.to));
        bindPositionAndPiece(opt.to, opt.oldPiece);
        if (opt.oldPiece != null) {
            getAlivePieceCounter(opt.oldPiece.getColor()).add(opt.oldPiece);
        }
        refreshPieces(opt.from, opt.to);
    }

    /**
     * 刷新棋子信息
     */
    private void refreshPieces(Position... positions) {
        aliveRedPieces.forEach(piece -> piece.refresh(positions));
        aliveBlackPieces.forEach(piece -> piece.refresh(positions));
    }

    /**
     * 获取某一方的所有存活的棋子
     */
    public PieceCounter getAllPieces(Color color) {
        return getAlivePieceCounter(color);
    }

    /**
     * 获取某一方的存活的某类棋子
     */
    public List<Piece> getPieces(Color color, Role role) {
        return getAlivePieceCounter(color).get(role);
    }

    /**
     * 获取某一方的存活的符合指定要求的棋子
     */
    public Iterator<Piece> getPieces(Color color, Predicate<Piece> filter) {
        return new FilterIterator<>(getAllPieces(color).iterator(), filter);
    }

    /**
     * 获取某一方的将/帅
     */
    public Piece getKing(Color color) {
        return getPieces(color, Role.KING).get(0);
    }

    /**
     * 判断某一方是否被将军，即该方的将帅是否被攻击
     */
    public boolean isChecked(Color color) {
        return !getKing(color).getAttackers().isEmpty();
    }

    /**
     * 获取某一方所有的能够到将的动作
     */
    public Iterator<Action> getCheckActions(Color color) {
        Piece opponentKing = getKing(color.reverse());
        List<Piece> aggressivePieces = CollectionUtil.toList(getPieces(color, p -> p.getRole().aggressive));
        return new LazyMatrixIterator<>(aggressivePieces, p -> p.getAttackOrProtectActions(opponentKing));
    }

    public int getActionCount() {
        return operations.size();
    }

    public Action getAction(int idx) {
        return operations.get(idx);
    }

    public boolean isLast(int x, int y) {
        if (operations.isEmpty()) {
            return false;
        }
        Operation last = operations.get(operations.size() - 1);
        Position target = Position.of(x, y);
        return target == last.from || target == last.to;
    }

    /**
     * 获取指定下标的棋子
     */
    @Nullable
    public Piece get(Position position) {
        return board[position.x][position.y];
    }

    /**
     * 创建一个棋子
     */
    private Piece newPiece(Color color, Role role, Position position) {
        switch (role) {
            case SOLDIER:  return new SoldierPiece(color, position, this);
            case CANNON:   return new CannonPiece(color, position, this);
            case CHARIOT:  return new ChariotPiece(color, position, this);
            case HORSE:    return new HorsePiece(color, position, this);
            case ELEPHANT: return new ElephantPiece(color, position, this);
            case GUARD:    return new GuardPiece(color, position, this);
            case KING:     return new KingPiece(color, position, this);
            default:       throw new IllegalArgumentException();
        }
    }

    private void bindPositionAndPiece(Position position, @Nullable Piece piece) {
        board[position.x][position.y] = piece;
        modifiableSnapshot.set(position, piece);
        if (piece != null) {
            piece.setPosition(position);
        }
    }

    private PieceCounter getAlivePieceCounter(Color color) {
        return color == Color.RED ? aliveRedPieces : aliveBlackPieces;
    }

    public Snapshot getSnapshot() {
        return new Snapshot(modifiableSnapshot.data);
    }


    //
    // ====================== 非核心代码 ======================
    //

    /**
     * 棋盘可视化
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Piece[] row : board) {
            for (Piece piece : row) {
                sb.append(piece == null ? "〇" : piece).append(' ');
            }
            sb.setCharAt(sb.length() - 1, '\n');
        }
        return sb.toString();
    }

    /**
     * 测试程序
     */
    public static void main(String[] args) {

        // 通过快照恢复棋盘
        Chess chess = new Chess();
        chess = chess.getSnapshot().resume();
        System.out.println(chess);

        // 判断实时计算快照的逻辑是否有误
        System.out.println(chess.getSnapshot().equals(new Snapshot(chess.board)));
    }
}
